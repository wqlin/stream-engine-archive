package com.me.mqtt.client

import org.eclipse.paho.client.mqttv3.MqttConnectOptions

/**
 * Created by wqlin on 18-4-3 19:01.
 */
class Clients {
    static IMqttClient getLogDataMqttClient() {
        Properties props = new Properties()
        props.load(LogDataMqttClient.class.getClassLoader().getResourceAsStream("mqtt-client.properties"))
        String topic = props.getProperty("mqtt.topic")
        String brokerUrl = props.getProperty("brokerUrl")
        String clientId = props.getProperty("clientId")
        LogDataMqttClient client = new LogDataMqttClient(brokerUrl, clientId, topic)

        MqttConnectOptions options = new MqttConnectOptions()
        options.setCleanSession(Boolean.valueOf("connection.keepAliveInterval"))
        options.setKeepAliveInterval(Integer.valueOf(props.getProperty("connection.keepAliveInterval")))
        options.setConnectionTimeout(Integer.valueOf(props.getProperty("connection.connectionTimeout")))
        options.setAutomaticReconnect(Boolean.valueOf(props.getProperty("connection.automaticReconnect")))
        client.connect(options)
        return client
    }
}
