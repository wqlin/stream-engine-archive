package com.me.mqtt.client

import com.me.exceptions.MissingArgumentException
import org.eclipse.paho.client.mqttv3.MqttConnectOptions

/**
 * Created by wqlin on 18-4-10 10:16.
 * Green 采集系统向云端 Kafka 抛数据调用接口
 */
interface IMqttClient {
    void connect()

    void connect(MqttConnectOptions options)

    void publish(List<Map<String, Object>> dataList) throws MissingArgumentException, IOException

    void disconnect()
}
