package com.me.mqtt.client

import com.me.entity.LogData
import com.me.exceptions.MissingArgumentException
import static com.me.utils.Utils.*
import org.apache.avro.io.BinaryEncoder
import org.apache.avro.io.DatumWriter
import org.apache.avro.io.EncoderFactory
import org.apache.avro.specific.SpecificDatumWriter
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

/**
 * Created by wqlin on 18-4-3 13:43.
 */
class LogDataMqttClient extends AbstractMqttClient {
    private static final Logger logger = LogManager.getLogger(LogDataMqttClient.class)

    private DatumWriter<LogData> logDataWriter = new SpecificDatumWriter<LogData>(LogData.getClassSchema())

    private BinaryEncoder logDataBinaryEncoder = null

    LogDataMqttClient(String brokerUrl, String clientId, String topic) {
        super(brokerUrl, clientId, topic)
    }

    private
    static LogData getLogData(Map<String, Object> data) throws MissingArgumentException {
        if (data.get("uuid") == null)
            throw new MissingArgumentException("缺少 uuid")
        else if (data.get("device_id") == null)
            throw new MissingArgumentException("缺少 device_id")
        else if (data.get("log_addr") == null)
            throw new MissingArgumentException("缺少 log_addr")
        else if (data.get("min_value") == null)
            throw new MissingArgumentException("缺少 min_value")
        else if (data.get("max_value") == null)
            throw new MissingArgumentException("缺少 max_value")
        else if (data.get("exceed") == null)
            throw new MissingArgumentException("缺少 exceed")
        else if (data.get("periodic_at") == null)
            throw new MissingArgumentException("缺少 periodic_at")

        long periodic_at = (long) data.get("periodic_at")
        Calendar calendar = Calendar.getInstance()
        calendar.setTimeInMillis(periodic_at)
        // 忽略秒和毫秒
        calendar.clear(Calendar.SECOND)
        calendar.clear(Calendar.MILLISECOND)
        int minute = calendar.get(Calendar.MINUTE)

        // 确保分钟一定是 0, 15, 30, 45
        if (minute != 0 && minute != 15 && minute != 30 && minute != 45) {
            if (minute > 0 && minute <= 10)
                minute = 0
            else if (minute > 10 && minute <= 20)
                minute = 15
            else if (minute > 20 && minute <= 40)
                minute = 30
            else if (minute > 40)
                minute = 45
            calendar.set(Calendar.MINUTE, minute)
        }

        LogData logData = LogData
                .newBuilder()
                .setUuid((String) data.get("uuid"))
                .setDeviceId((String) data.get("device_id"))
                .setLogAddr((String) data.get("log_addr"))
                .setAccValue(Double.parseDouble((String) data.get("value")))
                .setValue(0.0)
                .setMinValue(Double.parseDouble((String) data.get("min_value")))
                .setMaxValue(Double.parseDouble((String) data.get("max_value")))
                .setExceed((int) data.get("exceed"))
                .setPeriodicAt(calendar.getTimeInMillis())
                .setSource(0)
                .build()
        return logData
    }

    private void publishOne(Map<String, Object> data) {
        LogData logData = getLogData(data)
        ByteArrayOutputStream out = new ByteArrayOutputStream()
        logDataBinaryEncoder = EncoderFactory.get().binaryEncoder(out, logDataBinaryEncoder)
        logDataWriter.write(logData, logDataBinaryEncoder);
        logDataBinaryEncoder.flush()
        out.close()
        byte[] payload = out.toByteArray()
        publish(getTopic(), payload)
        logger.info("[新消息]: " + LogData2Str(logData))
    }

    /**
     * 向 MQTT Broker publish 数据
     * @param dataList
     * @throws MissingArgumentException 缺少了某个参数时抛出
     * @throws IOException publish 失败时抛出
     */
    @Override
    void publish(List<Map<String, Object>> dataList) throws MissingArgumentException, IOException {
        for (Map<String, Object> data : dataList)
            publishOne(data)
        logger.info("发送完成...")
    }

    public static void main(String[] args) {
        long timestamp = 1523802612345L
        Calendar calendar = Calendar.getInstance()
        calendar.setTimeInMillis(timestamp)
        calendar.clear(Calendar.SECOND)
        calendar.clear(Calendar.MILLISECOND)
        println calendar.getTimeInMillis()
        println calendar.format("yyyy-MM-dd HH:mm:ss:SSS")
    }
}
