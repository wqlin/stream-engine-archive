package com.me.mqtt.client

import org.eclipse.paho.client.mqttv3.MqttClient
import org.eclipse.paho.client.mqttv3.MqttConnectOptions
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence

/**
 * Created by wqlin on 18-4-3 17:11.
 */
abstract class AbstractMqttClient implements IMqttClient {
    private MqttClient client

    private String topic

    AbstractMqttClient(String brokerUrl, String clientId, String topic) {
        setTopic(topic)
        client = new MqttClient(brokerUrl, clientId, new MemoryPersistence())
    }

    void setTopic(String topic) {
        this.topic = topic
    }

    String getTopic() {
        return topic
    }

    void connect() {
        MqttConnectOptions options = new MqttConnectOptions()
        options.setCleanSession(true)
        options.setAutomaticReconnect(true)
        options.setConnectionTimeout(600)
        options.setKeepAliveInterval(30)
        this.connect(options)
    }

    void connect(MqttConnectOptions options) {
        client.connect(options)
    }

    boolean isConnected() {
        return client.isConnected()
    }

    // at most once (0), at least once (1), Exactly once (2)
    protected void publish(String topic, byte[] payload, int qos = 1, boolean retained = false) throws IllegalStateException {
        client.publish(topic, payload, qos, retained)
    }

    void disconnect() {
        client.disconnect()
    }
}
