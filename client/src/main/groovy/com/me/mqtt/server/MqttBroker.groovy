package com.me.mqtt.server

import com.google.common.collect.Lists
import io.moquette.BrokerConstants
import io.moquette.interception.AbstractInterceptHandler
import io.moquette.interception.messages.InterceptPublishMessage
import io.moquette.server.Server
import io.moquette.server.config.MemoryConfig
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger


/**
 * Created by wqlin on 18-4-3 13:50.
 */
class MqttBroker {
    private static final Logger logger = LogManager.getLogger(MqttBroker.class)
    MemoryConfig config

    Server broker

    MqttBroker() {
        Properties properties = new Properties()
        properties.put(BrokerConstants.HOST, "0.0.0.0")
        properties.put(BrokerConstants.PORT_PROPERTY_NAME, "8883")
        properties.put(BrokerConstants.ALLOW_ANONYMOUS_PROPERTY_NAME, "true")
        config = new MemoryConfig(properties)
    }

    void startServer() throws IOException {
        broker.startServer(config, Lists.asList(new InterceptHandler()))
    }

    private static class InterceptHandler extends AbstractInterceptHandler {
        @Override
        void onPublish(InterceptPublishMessage msg) {
            if (msg != null) {
                String message = msg.payload.array()
                logger.info("MQTT Broker 收到消息: " + message + ", 消息主题为: " + msg.getTopicName())
            } else {
                logger.warn("MQTT Broker 收到空消息.")
            }
        }
    }
}
