package com.me.exceptions;

/**
 * Created by wqlin on 18-4-1 23:22.
 */
public class MissingArgumentException extends Exception {
    public MissingArgumentException() {
    }

    public MissingArgumentException(String message) {
        super(message);
    }
}
