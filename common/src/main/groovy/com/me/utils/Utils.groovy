package com.me.utils

import com.me.entity.LogData
import com.me.entity.ManagementEntity

import java.math.RoundingMode

/**
 * Created by wqlin on 18-4-18 08:49.
 */
class Utils {
    static String milliSecond2DateStr(long millisecond) {
        Calendar calendar = Calendar.getInstance()
        calendar.setTimeInMillis(millisecond)
        return calendar.format("yyyy-MM-dd HH:mm:ss:SSS")
    }

    static String LogData2Str(LogData logData) {
        return "设备 ID: " + logData.getDeviceId() + ", 累计读数: " + logData.getAccValue() +
                ", 能耗值: " + logData.getValue() + ", 来源: " + (logData.source == 0 ? "采集" : "插值") +
                ", 时间戳: " + milliSecond2DateStr(logData.getPeriodicAt())
    }

    static String ME2Str(ManagementEntity record) {
        return "实体 ID: " + record.getEntityId() + ", 能耗: " + record.getValue() +
                ", 时间戳: " + milliSecond2DateStr(record.getTimestamp()) +
                ", 上一次更新时间戳: " + milliSecond2DateStr(record.getLastUpdate())
    }

    // 对 double 做 round, 保留
    // 参考: https://stackoverflow.com/questions/2808535/round-a-double-to-2-decimal-places
    public static double round(double value, int scale = 4) {
        if (scale < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value)
        bd = bd.setScale(scale, RoundingMode.HALF_UP)
        return bd.doubleValue()
    }
}
