package com.me.utils

import com.google.common.collect.Lists

/**
 * Created by wqlin on 18-4-24 11:02.
 */
public class TableManager {
    public static enum TableType {
        Hour,
        Day,
        Month,
        Year
    }

    private static final String HOUR_TABLE_PREFIX = "t_m_hour_"
    private static final int HOUR_TABLE_COUNT = 10

    private static final String DAY_TABLE_PREFIX = "t_m_day"
    private static final String MONTH_TABLE_PREFIX = "t_m_month"
    private static final String YEAR_TABLE_PREFIX = "t_m_year"

    /**
     * 根据 entityId 找到该 ID 所属表
     * @param entityId
     * @param type
     * @return
     */
    static String getTable(String entityId, TableType type) throws IllegalArgumentException {
        String tableName
        switch (type) {
            case TableType.Hour:
                int postfix = entityId.hashCode() % HOUR_TABLE_COUNT
                tableName = HOUR_TABLE_PREFIX + String.valueOf(postfix)
                break
            case TableType.Day:
                tableName = DAY_TABLE_PREFIX
                break
            case TableType.Month:
                tableName = MONTH_TABLE_PREFIX
                break
            case TableType.Year:
                tableName = YEAR_TABLE_PREFIX
                break
            default:
                throw new IllegalArgumentException("Illegal table type")
        }
        return tableName
    }
    /**
     * @return 所有的小时数据表 , 下面类似
     */
    static List<String> getHourTables() {
        ArrayList<String> result = new ArrayList<>()
        for (int i = 0; i < HOUR_TABLE_COUNT; i++) {
            String tableName = HOUR_TABLE_PREFIX + String.valueOf(i)
            result.add(tableName)
        }
        return result
    }

    static List<String> getDayTables() {
        return Lists.asList(DAY_TABLE_PREFIX)
    }

    static List<String> getMonthTables() {
        return Lists.asList(MONTH_TABLE_PREFIX)
    }

    static List<String> getYearTables() {
        return Lists.asList(YEAR_TABLE_PREFIX)
    }
}
