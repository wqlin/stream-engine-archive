package com.me;

/**
 * Created by wqlin on 18-4-17 22:12.
 */
public class Constants {
    public static final String SCHEMA_REGISTRY_URL = "http://localhost:8081"
    public static final String BOOTSTRAP_SERVERS_CONFIG = "localhost:9092"

    public static final String RAW_DATA_TOPIC = "k_minute_raw"
    public static final String MATURE_DATA_TOPIC = "k_minute_mature"

    // 管理实体逐时, 逐日, 逐月和逐年的主题
    public static final String ME_MINUTE_DATA_TOPIC = "k_m_minute"

    public static final String ME_HOUR_DATA_TOPIC = "k_m_hour"

    public static final String ME_DAY_DATA_TOPIC = "k_m_day"

    public static final String ME_MONTH_DATA_TOPIC = "k_m_month"

    public static final String ME_YEAR_DATA_TOPIC = "k_m_year"
}
