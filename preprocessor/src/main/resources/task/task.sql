DROP DATABASE IF EXISTS energy_test_data;
CREATE DATABASE energy_test_data;
USE energy_test_data;
CREATE TABLE hour_data (
  id        INT AUTO_INCREMENT,
  deviceId  VARCHAR(13),
  data      DOUBLE(20, 2),
  epoch     BIGINT, # 从 UNIX epoch 至今的纳秒
  timestamp DATETIME,
  PRIMARY KEY (id)
);

CREATE TABLE day_data (
  id        INT AUTO_INCREMENT,
  deviceId  VARCHAR(13),
  data      DOUBLE(20, 2),
  epoch     BIGINT,
  timestamp DATETIME,
  PRIMARY KEY (id)
);

CREATE TABLE month_data (
  id        INT AUTO_INCREMENT,
  deviceId  VARCHAR(13),
  data      DOUBLE(20, 2),
  epoch     BIGINT,
  timestamp DATETIME,
  PRIMARY KEY (id)
);

CREATE TABLE year_data (
  id        INT AUTO_INCREMENT,
  deviceId  VARCHAR(13),
  data      DOUBLE(20, 2),
  epoch     BIGINT,
  timestamp DATETIME,
  PRIMARY KEY (id)
);