package com.me.preprocessor

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.ZoneId

/**
 * Created by wqlin on 18-3-29 11:27.
 */
class PreprocessorUtils {
    static final DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

    static List<String> getHourTablesForYear(int year) {
        final String prefix = " t_energy_hourdata_"
        List<String> postfix = Arrays.asList("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12")
        return postfix.stream().map { str -> prefix + year + str }.collect()
    }

    static void main(String[] args) {
        (2015..2017).forEach { year ->
            List<String> hourTables = getHourTablesForYear(year)
            hourTables.forEach { table -> System.out.println(table) }
        }
    }
}
