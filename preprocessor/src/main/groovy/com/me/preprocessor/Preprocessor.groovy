package com.me.preprocessor

import com.google.common.collect.Lists
import com.google.common.collect.Sets
import com.me.mqtt.client.Clients
import com.me.mqtt.client.IMqttClient
import com.me.mysql.entity.Triple
import com.me.mysql.mapper.TripleMapper
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import java.time.YearMonth
import java.util.stream.Collectors

/**
 * Created by wqlin on 18-4-2 09:33.
 * 执行将 MySQL 写入 Kafka 的过程
 */
@Component("Preprocessor")
class Preprocessor {
    private static final Logger logger = LogManager.getLogger("Processor")

    @Autowired
    private TripleMapper tripleMapper

    IMqttClient client = Clients.getLogDataMqttClient()

    Preprocessor() {
    }

    public static Map<String, Object> getRawMap(Triple t) {
        Map<String, Object> res = new HashMap<>()
        res.put("uuid", "")
        res.put("device_id", t.getDevice_id())
        res.put("log_addr", "")
        res.put("value", String.valueOf(t.getValue()))
        res.put("min_value", String.valueOf(0.0))
        res.put("max_value", String.valueOf(0.0))
        res.put("exceed", new Integer(0))
        res.put("periodic_at", t.getPeriodic_at())
        return res
    }

    private static int getLengthOfMonth(int year, int month) {
        YearMonth yearMonth = YearMonth.of(year, month)
        return yearMonth.lengthOfMonth()
    }

    private static String getStartTimeForDay(int year, int month, int day) {
        return year + "-" + String.format("%02d", month) + "-" + String.format("%02d", day) + " 00:00:00"
    }

    private static String getEndTimeForDay(int year, int month, int day) {
        return year + "-" + String.format("%02d", month) + "-" + String.format("%02d", day) + " 23:59:59"
    }

    private static final List<String> deviceIds = Lists.asList(
            "A6B525254CCD45049315D5CD9FFBF492",
            "F4E4BE9E51F24E71BE3B140C2E20B0B0",
            "E267B516E353445F8F044321DBAF1AA6"
//            "E69F9CFB583A4699AF22684630833BCE",
//            "C0A537364DEF4ECFABCDC9A9CF51B6A4",
//            "D950772B0311423EAB4D948FC8992933",
//            "B006B94EF8B84F01825CAF29A8C05F94",
//            "A5CFF7CFDF5D4E688F5F368EF0C28283",
//            "ABCF9A32F0D74FF78C7AEB4AAF5EDF4E",
//            "BC24D112FC86404691473AB51DA99DE3",
//            "D7D0686938884DC280187EEC160EF96F",
//            "F73D4F1EC02248098F644EE75C16FF08",
//            "E1BDDA864D5E48D48E603F14E532BB7F",
//            "EAD96264DF6D48B491500B623A0448E0",
//            "EF78487DEE5043C29287C0E768C5174F"
    )

    private void checkTotal() {
        deviceIds.forEach { deviceId ->
            List<Integer> deviceTotals = new ArrayList<>()
            (2016..2017).forEach { year ->
                List<String> tables = PreprocessorUtils.getHourTablesForYear(year)
                List<Integer> totals = tables.stream().map { table ->
                    tripleMapper.getTotal(table, deviceId)
                }.collect(Collectors.toList())
                deviceTotals.addAll(totals)
            }
            if (deviceTotals.stream().allMatch { it -> it > 0 }) {
                println deviceId
            }
        }
    }


    void processDataByDay(int year) {
        List<String> tables = PreprocessorUtils.getHourTablesForYear(year)
        for (int i = 0; i < 12; i++) {
            int month = i + 1
            String table = tables.get(i)
            int lengthOfMonth = getLengthOfMonth(year, month)
            for (int day = 1; day <= lengthOfMonth; day++) {
                String startTime = getStartTimeForDay(year, month, day)
                String endTime = getEndTimeForDay(year, month, day)
                for (String deviceId : deviceIds) {
                    logger.info("读取新纪录, table: " + table + ", deviceId: " + deviceId + ", startTime: " + startTime + ", endTime: " + endTime)
                    try {
                        List<Triple> tripleList = tripleMapper.getTripleWithinTimeRange(table, deviceId, startTime, endTime)
                        logger.info("读取成功...")
                        List<Map<String, Object>> dataList =
                                tripleList.stream()
                                        .filter { it.getDevice_id().length() > 2 }
                                        .map { getRawMap(it) }
                                        .collect(Collectors.toList())
                        client.publish(dataList)
                        logger.info("新纪录 publish 完成, size: " + dataList.size())
                        Thread.sleep(1000 * 1)
                    } catch (Exception e) {
                        logger.error(e)
                    }
                }
                Thread.sleep(1000 * 1)
            }
        }
    }

    void start() {
        (2016..2017).forEach { year ->
            processDataByDay(year)
        }
    }
}
