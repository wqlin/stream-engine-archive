package com.me.mysql.mapper

import com.me.mysql.entity.EnergyDataDevice
import org.springframework.stereotype.Component

/**
 * Created by wqlin on 18-3-21 10:54.
 */
@Component
@Deprecated
interface EnergyDataDeviceMapper {
    List<EnergyDataDevice> get() throws Exception
}