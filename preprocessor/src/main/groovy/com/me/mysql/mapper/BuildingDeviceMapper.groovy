package com.me.mysql.mapper

import com.me.mysql.entity.BuildingDevice
import org.springframework.stereotype.Component

/**
 * Created by wqlin on 18-3-21 10:47.
 */
@Component
interface BuildingDeviceMapper {
    List<BuildingDevice> get() throws Exception
}