package com.me.mysql.mapper

import com.me.mysql.entity.FloorDevice
import org.springframework.stereotype.Component

/**
 * Created by wqlin on 18-3-21 10:52.
 */
@Component
interface FloorDeviceMapper {
    List<FloorDevice> get() throws Exception
}