package com.me.mysql.mapper

import com.me.mysql.entity.Triple
import org.apache.ibatis.annotations.Param
import org.springframework.stereotype.Component

/**
 * Created by wqlin on 18-4-3 19:45.
 */
@Component
interface TripleMapper {
    List<String> getDeviceIdAt(@Param("tableName") String tableName)

    List<Triple> getTripleByDeviceIdAt(@Param("tableName") String tableName,
                                       @Param("deviceId") String deviceId)

    List<Triple> getTripleWithinTimeRange(@Param("tableName") String tableName,
                                          @Param("deviceId") String deviceId,
                                          @Param("startTime") String startTime,
                                          @Param("endTime") String endTime)

    Integer getTotal(@Param("tableName") String tableName,
                     @Param("deviceId") String deviceId)
}
