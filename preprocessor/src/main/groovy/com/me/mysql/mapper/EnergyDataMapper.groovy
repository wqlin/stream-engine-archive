package com.me.mysql.mapper;

import com.me.mysql.entity.EnergyData;
import com.me.mysql.entity.EnergyHourData;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by wqlin on 18-3-22 15:12.
 */
@Component
@Deprecated
public interface EnergyDataMapper {
    List<String> getDeviceId(@Param("tableName") String tableName);

    List<EnergyHourData> get(@Param("tableName") String tableName);

    List<EnergyHourData> getHourDataByDeviceId(@Param("deviceId") String deviceId,
                                               @Param("tableName") String tableName);

    List<EnergyData> getDayDataByDeviceId(@Param("deviceId") String deviceId,
                                          @Param("year") int year);

    List<EnergyData> getMonthDataByDeviceId(@Param("deviceId") String deviceId,
                                            @Param("year") int year);

    List<EnergyData> getYearDataByDeviceId(@Param("deviceId") String deviceId,
                                           @Param("year") int year);
}