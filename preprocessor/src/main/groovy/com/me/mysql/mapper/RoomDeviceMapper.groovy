package com.me.mysql.mapper

import com.me.mysql.entity.RoomDevice
import org.springframework.stereotype.Component

/**
 * Created by wqlin on 18-3-21 10:53.
 */
@Component
interface RoomDeviceMapper {
    List<RoomDevice> get() throws Exception
}