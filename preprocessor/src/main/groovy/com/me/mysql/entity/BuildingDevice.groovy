package com.me.mysql.entity

/**
 * Created by wqlin on 18-3-21 09:34.
 * 建筑与设备映射关系, 对应 t_build_device 表
 */
class BuildingDevice {
    int buildId // 建筑 ID
    String deviceId //设备 ID

    @Override
    String toString() {
        return "建筑编号: " + buildId +
                "\t设备编号: " + deviceId
    }
}
