package com.me.mysql.entity

/**
 * Created by wqlin on 18-3-21 10:16.
 * 房间与设备映射关系, 对应 t_room_device 表
 */
class RoomDevice {
    int roomId // 房间 ID
    String deviceId // 设备 ID

    @Override
    String toString() {
        return "房间编号: " + roomId +
                "\t设备编号: " + deviceId
    }
}
