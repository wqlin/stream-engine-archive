package com.me.mysql.entity

/**
 * Created by wqlin on 18-3-21 09:41.
 * 部门与设备映射关系, 对应 t_department_device 表
 */
class DepartmentDevice {
    int departmentId // 部门 ID
    String deviceId // 设备 ID
    @Override
    String toString() {
        return "部门编号: " + departmentId +
                "\t设备编号: " + deviceId
    }
}
