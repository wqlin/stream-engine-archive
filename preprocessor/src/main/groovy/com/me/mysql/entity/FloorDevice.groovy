package com.me.mysql.entity
/**
 * Created by wqlin on 18-3-21 10:12.
 * 楼层与设备映射关系, 对应 t_floor_device
 */
class FloorDevice {
    int layerId // 楼层 ID
    String deviceId // 设备 ID

    @Override
    String toString() {
        return "楼层编号: " + layerId +
                "\t设备编号: " + deviceId
    }
}
