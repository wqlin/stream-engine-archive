package com.me.mysql.entity

/**
 * Created by wqlin on 18-3-22 14:38.
 * 对应 t_energy_daydata, t_energy_monthdata
 * 和 t_energy_yeardata
 */
class EnergyData {
    String DeviceId
    double Energy_data
    Date Update_Time

    String toString() {
        return "设备编号: " + DeviceId +
                "\t能耗数据: " + Energy_data +
                "\t更新时间: " + Update_Time
    }
}
