package com.me.mysql.entity

/**
 * Created by wqlin on 18-3-20 14:55.
 * 对应能耗数据小时数据, t_energy_hourdata_****
 */
class EnergyHourData {
    String DeviceId // 设备 ID
    Date Read_Time // 采集时间
    double Pre_Value  // 电表上一次读数, 可能为 0
    double Read_Value // 电表读数, 可能为 0
    double Energy_data // 能耗数值 = Read_Value - Pre_Value
    double TotalValue // 总数

    @Override
    String toString() {
        return "设备编号: " + DeviceId +
                "\t上一次读数: " + Pre_Value +
                "\t本次读数: " + Read_Value +
                "\t能耗数值: " + Energy_data +
                "\t总数: " + TotalValue +
                "\t采集时间: " + Read_Time
    }
}
