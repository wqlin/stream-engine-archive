package com.me.mysql.entity

/**
 * Created by wqlin on 18-3-21 10:08.
 * 分项与设备映射关系, 对应 t_energydata_device 表
 */
class EnergyDataDevice {
    String energyId // 分项 ID
    String deviceId // 设备 ID

    @Override
    String toString() {
        return "分项编号: " + energyId +
                "\t设备编号: " + deviceId
    }
}
