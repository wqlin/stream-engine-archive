package com.me.mysql.entity

import static com.me.utils.Utils.milliSecond2DateStr

/**
 * Created by wqlin on 18-4-8 15:50.
 */
class Triple {
    String device_id = "null"
    double value = 0.0
    long periodic_at = System.currentTimeMillis() // 所属周期

    void setRead_Time(Long Read_Time) {
        this.periodic_at=Read_Time
    }

    String toString() {
        return "设备编号: " + device_id +
                "\t数值: " + String.format("%.4f", value) +
                "\t周期时间: " + milliSecond2DateStr(periodic_at)
    }
}
