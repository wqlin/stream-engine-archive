package com.me.mysql.task.entity

import java.sql.Timestamp

/**
 * Created by wqlin on 18-3-22 11:29.
 * 浩宇数据
 */
class Record {
    Record(String deviceId, double energyData, long epoch, Date timeStamp) {
        this.deviceId = deviceId
        this.energyData = energyData
        this.epoch = epoch
        this.timestamp = timeStamp
    }

    Record(String deviceId, double energyData, long epoch, Timestamp timeStamp) {
        this.deviceId = deviceId
        this.energyData = energyData
        this.epoch = epoch
        this.timestamp = timeStamp
    }

    String deviceId // 设备编号
    double energyData // 能耗数据
    long epoch // 时间戳, 从 UNIX epoch 开始的纳秒时刻
    Date timestamp

    String toString() {
        return "设备编号: " + deviceId +
                "\t能耗数据: " + energyData +
                "\t纳秒: " + epoch +
                "\t时间戳: " + timestamp
    }
}
