package com.me.mysql.task.mapper

import com.me.mysql.task.entity.Record
import org.apache.ibatis.annotations.Options
import org.apache.ibatis.annotations.Param
import org.springframework.stereotype.Component

/**
 * Created by wqlin on 18-3-22 11:37.
 */
@Component
interface RecordMapper {
    @Options(useGeneratedKeys = true, flushCache = Options.FlushCachePolicy.TRUE)
    int insert(@Param("tableName") String tableName,
               @Param("record") Record record)

    List<Record> getByDeviceId(@Param("tableName") String tableName,
                               @Param("deviceId") String deviceId)

    List<Record> getByDeviceIdInTimeRange(@Param("tableName") String tableName,
                                          @Param("deviceId") String deviceId,
                                          @Param("fromDate") String fromDate,
                                          @Param("toDate") String toDate)

    Double sumByDeviceId(@Param("tableName") String tableName,
                         @Param("deviceId") String deviceId)
}
