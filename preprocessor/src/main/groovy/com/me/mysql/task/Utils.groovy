package com.me.mysql.task

import java.time.LocalDateTime
import java.time.Month
import java.time.ZoneId

/**
 * Created by wqlin on 18-3-22 13:33.
 */
class Utils {

    private static final String PREFIX = "000010027"

    public static final int YEAR = 2016

    public static final String HOUR_DATA_TABLE = "hour_data"
    public static final String DAY_DATA_TABLE = "day_data"
    public static final String MONTH_DATA_TABLE = "month_data"
    public static final String YEAR_DATA_TABLE = "year_data"

    public static
    final Date startDate = Date.from(LocalDateTime.of(YEAR-1, Month.DECEMBER, 31, 23, 59).atZone(ZoneId.of("UTC")).toInstant());
    public static
    final Date endDate = Date.from(LocalDateTime.of(YEAR + 1, Month.JANUARY, 1, 0, 0).atZone(ZoneId.of("UTC")).toInstant());

    /**
     * 根据传入的序列号返回新的设备编码
     * @return 新的设备编号
     */
    static String getDeviceId(int no) {
        StringBuffer ss = new StringBuffer(no.toString())
        while (ss.length() < 4)
            ss.insert(0, '0')
        return PREFIX + ss.toString()
    }

    static long toEpoch(Date date) {
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
//        Date date = format.parse(time)
        return date.getTime()
    }

    static List<String> getHourTables(int year) {
        String prefix = " t_energy_hourdata_"
        List<String> postfix = Arrays.asList("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12")
        String y = year.toString()
        return postfix.stream().map { str -> prefix + y + str }.collect()
    }

    static void main(String[] args) {
//        (1..100).each {
//            println getDeviceId(it)
//        }
        //println toEpoch("2016-01-01 00:00:19")
        List<String> l = getHourTables(YEAR)
        l.stream().forEach { it -> System.out.println(it) }
    }
}
