package com.me.mysql.task;

import com.me.mysql.entity.EnergyData;
import com.me.mysql.entity.EnergyHourData;
import com.me.mysql.mapper.EnergyDataMapper;
import com.me.mysql.task.entity.Record;
import com.me.mysql.task.mapper.RecordMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.BiFunction;

import static java.util.Calendar.YEAR;
import static com.me.mysql.task.Utils.*;

/**
 * Created by wqlin on 18-3-22 11:28.
 * 不需要看...
 */
@Component
public class Task {
    private static Logger logger = LogManager.getLogger(Task.class);

    private String device_csv = "task/deviceId.csv";

    @Autowired
    private EnergyDataMapper energyDataMapper;

    @Autowired
    private RecordMapper recordMapper;

    public Task(EnergyDataMapper energyDataMapper,
                RecordMapper recordMapper) {
        this.energyDataMapper = energyDataMapper;
        this.recordMapper = recordMapper;
    }

    private int getLengthOfMonth(int month) {
        LocalDate data = LocalDate.of(YEAR, month, 1);
        return data.lengthOfMonth();
    }

    private int getMonthOfTable(String table) {
        return Integer.valueOf(table.substring(table.length() - 2, table.length()));
    }
    private static List<EnergyHourData> toHourData(Record dayData, int month, int day) {
        double aver = dayData.getEnergyData() / 24;
        List<EnergyHourData> res = new ArrayList<>();
        double acc = 0.0;
        for (int i = 0; i <= 23; i++) {
            EnergyHourData data = new EnergyHourData();
            if (i == 23) data.setEnergy_data(dayData.getEnergyData() - acc);
            else data.setEnergy_data(aver);
            acc += aver;
            LocalDateTime date = LocalDateTime.of(YEAR, Month.values()[month - 1], day, i, 0);
            data.setRead_Time(Date.from(date.atZone(ZoneId.of("UTC")).toInstant()));
            res.add(data);
        }
        return res;
    }


    private List<EnergyHourData> handleHourDataMissing(String dstDeviceId, int month, int lengthOfMonth) {
        List<EnergyHourData> res = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:SS");
        String fromDate = LocalDateTime.of(YEAR, month, 1, 0, 0, 0).format(formatter);
        String toDate;
        if (month < 12)
            toDate = LocalDateTime.of(YEAR, month + 1, 1, 0, 0, 0).format(formatter);
        else
            toDate = LocalDateTime.of(YEAR + 1, 1, 1, 0, 0, 0).format(formatter);
        logger.info("month: " + month + " fromDate: " + fromDate + " toDate: " + toDate);
        List<Record> datDataList = recordMapper.getByDeviceIdInTimeRange("day_data", dstDeviceId, fromDate, toDate);
        assert datDataList.size() == lengthOfMonth;

        for (int i = 1; i <= lengthOfMonth; i++) {
            logger.info(datDataList.get(i - 1));
            res.addAll(toHourData(datDataList.get(i - 1), month, i));
        }
        return res;
    }

    /**
     * 处理小时表数据
     *
     * @param srcFun       EnergyDataMapper::getHourDataByDeviceId
     * @param srcDeviceId  设备编号
     * @param srcTableName hour data 表
     * @param dstFun       RecordMapper::insert
     * @return 成功插入的数量
     */
    private void processEnergyHourData(BiFunction<String, String, List<EnergyHourData>> srcFun,
                                       String srcDeviceId,
                                       String srcTableName,
                                       BiFunction<String, Record, Integer> dstFun,
                                       String dstDeviceId,
                                       String dstTableName) {
        int month = getMonthOfTable(srcTableName);
        int lengthOfMonth = getLengthOfMonth(month);
        List<EnergyHourData> hourData = srcFun.apply(srcDeviceId, srcTableName);
        if (hourData.size() != lengthOfMonth * 24) {
            // logger.warn(srcDeviceId + " 小时级表数据量: " + hourData.size());
            hourData = handleHourDataMissing(dstDeviceId, month, lengthOfMonth);
        }
        int rawCount = hourData.size();
        assert hourData.size() == lengthOfMonth * 24;
        int insertCount = hourData.stream().mapToInt(data ->
                dstFun.apply("hour_data", toRecord(dstDeviceId, data))).sum();
        // logger.info("源数据表: " + srcTableName + "\t读取数据量: " + rawCount + "\t目标数据库表: " + dstTableName + "\t插入数据量: " + insertCount);
        if (rawCount != insertCount)
            logger.warn("srcDeviceId:" + srcDeviceId + " srcTableName" + srcDeviceId + " 插入数据量: " + insertCount + " 不等于读取数据量: " + rawCount);
    }

    private void processEnergyData(BiFunction<String, Integer, List<EnergyData>> srcFun,
                                   String srcDeviceId,
                                   String srcTableName,
                                   BiFunction<String, Record, Integer> dstFun,
                                   String dstDeviceId,
                                   String dstTableName) {
        List<EnergyData> energyData = srcFun.apply(srcDeviceId, YEAR);
        int rawCount = energyData.size();
        // 年和月的数据应该是无误的
        switch (dstTableName) {
            case "year_data":
                assert (rawCount == 1);
                break;
            case "month_data":
                assert (rawCount == 12);
                break;
            default:
                assert (rawCount == 366);
                break;
        }
        int insertCount = energyData.stream().mapToInt(data ->
                dstFun.apply(dstTableName, toRecord(dstDeviceId, data))).sum();
        // logger.info("源数据表: " + srcTableName + "\t读取数据量: " + rawCount + "\t目标数据库表: " + dstTableName + "\t插入数据量: " + insertCount);
        if (rawCount != insertCount)
            logger.warn("srcDeviceId:" + srcDeviceId + " srcTableName" + srcDeviceId + " 插入数据量: " + insertCount + " 不等于读取数据量: " + rawCount);
    }


    private Record toRecord(String deviceId, EnergyHourData data) {
        assert data.getRead_Time().after(startDate);
        assert data.getRead_Time().before(endDate);
        return new Record(deviceId, data.getEnergy_data(), toEpoch(data.getRead_Time()), data.getRead_Time());
    }

    private Record toRecord(String deviceId, EnergyData data) {
        return new Record(deviceId, data.getEnergy_data(), toEpoch(data.getUpdate_Time()), data.getUpdate_Time());
    }

    public void process() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        String file = classLoader.getResource(device_csv).getFile();
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line;
        int deviceNo = 1;
        List<String> hourTables = getHourTables(YEAR); // hour table
        while ((line = reader.readLine()) != null) {
            String srcDeviceId = line.trim();
            String dstDeviceId = getDeviceId(deviceNo);

            // 处理年级数据
            processEnergyData(energyDataMapper::getYearDataByDeviceId,
                    srcDeviceId, "t_energy_yeardata",
                    recordMapper::insert,
                    dstDeviceId, "year_data");

            // 处理月级数据
            processEnergyData(energyDataMapper::getMonthDataByDeviceId,
                    srcDeviceId, "t_energy_monthdata",
                    recordMapper::insert,
                    dstDeviceId, "month_data");

            // 处理日级数据
            processEnergyData(energyDataMapper::getDayDataByDeviceId,
                    srcDeviceId, "t_energy_daydata_" + String.valueOf(YEAR),
                    recordMapper::insert,
                    dstDeviceId, "day_data");

            // 处理小时级数据
            hourTables.forEach(hourTable ->
                    processEnergyHourData(energyDataMapper::getHourDataByDeviceId,
                            srcDeviceId, hourTable, recordMapper::insert, dstDeviceId, "hour_data"));
            deviceNo += 1;
        }
        reader.close();
    }
}
