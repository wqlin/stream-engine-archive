package com.me.controller

import com.me.preprocessor.Preprocessor
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

import javax.annotation.PostConstruct
import java.util.concurrent.atomic.AtomicInteger

/**
 * Created by wqlin on 18-4-18 09:10.
 */
@Controller
class StartController {
    private static final Logger logger = LogManager.getLogger("Processor")

    private static final AtomicInteger count = new AtomicInteger(0)

    @Autowired
    Preprocessor preprocessor

    @PostConstruct
    void init() {
        if (count.incrementAndGet() == 1) {
            logger.info("Start running preprocessor...")
            preprocessor.start()
            logger.info("Preprocessor done")
        }
    }
}
