package com.me.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Created by wqlin on 18-4-1 22:14.
 */
@Configuration
@MapperScan({"com.me.mysql.mapper","com.me.mysql.task.mapper"})
public class MyBatisConfig {
    @Bean
    public DataSource dataSource() throws Exception {
        Properties props = new Properties();
        props.load(MyBatisConfig.class.getClassLoader().getResourceAsStream("jdbc.properties"));
        DruidDataSource ds = new DruidDataSource();
        ds.setDriverClassName(props.getProperty("jdbc.driver"));
        ds.setUrl(props.getProperty("jdbc.url"));
        ds.setUsername(props.getProperty("jdbc.username"));
        ds.setPassword(props.getProperty("jdbc.password"));
        ds.setInitialSize(5);
        return ds;
    }

    @Bean
    public DataSourceTransactionManager transactionManager() throws Exception {
        return new DataSourceTransactionManager(dataSource());
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setTypeAliasesPackage("com.me.mqtt.message");
        return sessionFactory.getObject();
    }
}
