package com.me.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by wqlin on 18-4-1 22:08.
 */
@Configuration
@ComponentScan({"com.me"})
public class RootConfig {
}

