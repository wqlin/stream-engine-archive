package com.me.mysql.mapper;

import com.me.config.RootConfig;
import com.me.mysql.entity.Triple;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

/**
 * Created by wqlin on 18-4-3 19:56.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {RootConfig.class})
public class LogDataMapperTest {
    @Autowired
    TripleMapper tripleMapper;
    private final String deviceId = "00ca47606caa46e7ad308caad4753a4f";
    private final String tableName = "t_energy_hourdata_201601";

    @Test
    public void testGetDeviceIdAt() throws Exception {
//        List<String> deviceIds = tripleMapper.getDeviceIdAt(tableName);
//        assert deviceIds != null;
    }

    @Test
    public void testGetTripleByDeviceIdAt() throws Exception {
//        List<String> deviceIds = tripleMapper.getDeviceIdAt(tableName);
//        assert deviceIds != null;
    }

    @Test
    public void testGetTripleWithinTimeRange() throws Exception {
        String startTime = "2016-01-01 00:00:00";
        String endTime = "2016-01-01 23:59:59";
        String deviceId = "F4E4BE9E51F24E71BE3B140C2E20B0B0";
        List<Triple> result = tripleMapper.getTripleWithinTimeRange(tableName, deviceId,startTime, endTime);
        result.forEach(System.out::println);
    }
}