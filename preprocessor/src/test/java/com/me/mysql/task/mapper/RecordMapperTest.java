package com.me.mysql.task.mapper;

import com.me.config.RootConfig;
import com.me.mysql.task.entity.Record;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Date;
import java.util.List;

import static com.me.mysql.task.Utils.*;

/**
 * Created by wqlin on 18-3-22 15:38.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {RootConfig.class})
public class RecordMapperTest {
    @Autowired
    private RecordMapper recordMapper;

//    @Test
//    public void testInsertHourData() throws Exception {
//        Record record = new Record("123", 10.0, 111, new Date());
//        assert recordMapper.insert(HOUR_DATA_TABLE, record) == 1;
//    }
//
//    @Test
//    public void testInsertDayData() throws Exception {
//        Record record = new Record("456", 10.0, 222,new Date());
//        int result = recordMapper.insert(DAY_DATA_TABLE, record);
//        System.out.println("result: " + result);
//        assert result == 1;
//    }
//
//    @Test
//    public void testInsertMonthData() throws Exception {
//        Record record = new Record("789", 10.0, 333, new Date());
//        assert recordMapper.insert(MONTH_DATA_TABLE, record) == 1;
//    }
//
//    @Test
//    public void testInsertYearData() throws Exception {
//        Record record = new Record("101112", 10.0, 444, new Date());
//        assert recordMapper.insert(YEAR_DATA_TABLE, record) == 1;
//    }
//
//    @Test
//    public void testGetByDeviceId() throws Exception{
//        List<Record> records=recordMapper.getByDeviceId("year_data","0000100270001");
//        records.forEach(System.out::println);
//    }
}