package com.me.mysql.task;

import com.me.config.RootConfig;
import com.me.mysql.task.mapper.RecordMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static com.me.mysql.task.Utils.*;

/**
 * Created by wqlin on 18-3-23 10:27.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {RootConfig.class})
public class TaskTest {
    @Autowired
    RecordMapper recordMapper;

    @Autowired
    Task task;

    // @Test
    public void testProcess() throws Exception {
    }

    // @Test
    public void testLengthOfMonth() {
        int sum = 0;
        for (int i = 1; i <= 12; i++) {
            LocalDate date = LocalDate.of(2016, i, 1);
            int days = date.lengthOfMonth();
            System.out.println(i + "th month has " + days + " days");
            sum += days;
        }
        System.out.println("Total days: " + sum);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:SS");
        String fromDate = LocalDateTime.of(YEAR, 1, 1, 0, 0, 1).format(formatter);
        System.out.println("fromDate: " + fromDate);
        String toDate = LocalDateTime.of(YEAR, 1, 31, 23, 59, 59).format(formatter);
        System.out.println("toDate: " + toDate);
    }

    // @Test
    public void testStringToInt() {
        List<String> ints = Arrays.asList("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
        ints.stream().map(Integer::valueOf).forEach(System.out::println);
        String table = "t_energy_hourdata_201608";
        System.out.println(Integer.valueOf(table.substring(table.length() - 2, table.length())));
    }

    public void testListForeachOrdered() {
        List<String> strings = Arrays.asList("1", "2", "3", "4", "5");
        Stream<String> stringStream = strings.stream();
        stringStream.forEachOrdered(System.out::println);
    }

    private boolean isEqual(double d1, double d2) {
        return Math.abs(d1 - d2) <= 0.001;
    }

    public void testSum() {
        for (int i = 1; i <= 100; i++) {
            String dstDeviceId = getDeviceId(i);
            double year_sum = recordMapper.sumByDeviceId("year_data", dstDeviceId);
            double month_sum = recordMapper.sumByDeviceId("month_data", dstDeviceId);
            double day_sum = recordMapper.sumByDeviceId("day_data", dstDeviceId);
            double hour_sum = recordMapper.sumByDeviceId("hour_data", dstDeviceId);
            System.out.println(dstDeviceId + " " + year_sum + " " + month_sum + " " + day_sum + " " + hour_sum);
        }
    }
}