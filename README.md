## 概述
建筑能耗大数据流计算引擎

## 开始
本项目使用 [Gradle](https://gradle.org/) 构建, 首先需要[下载安装 Gradle](https://gradle.org/install/).

除此之外, 本项目依赖 Confluent 平台, 安装参考: [Install Confluent Platform](https://docs.confluent.io/current/installation/installing_cp.html)

## 子模块
本项目分为以下几个子模块:
1. connector
2. fusion-engine
3. anomaly-detector


### connector
connector 目前只有向云端 Kafka 发送消息的功能, 运行参考 connector 模块下的 README

### fusion-engine
fusion-engine 


使用 Kafka connect 的时候, 需要先开启 MQTT Broker，再开启 Kafka MQTT Connector.


执行下面的命令, 使用 confluent 平台开启 connector:
```bash
gradle -q load
```

成功加载后, 应该看到下面的输出:
```bash
{"name":"mqtt-source","config":{"connector.class":"com.datamountaineer.streamreactor.connect.mqtt.source.MqttSourceConnector","tasks.max":"1","connect.mqtt.connection.clean":"true","connect.mqtt.connection.timeout":"30000","connect.mqtt.keep.alive":"30000","connect.mqtt.client.id":"mqtt-energy-consumer","connect.mqtt.converter.throw.on.error":"true","connect.mqtt.hosts":"tcp://120.76.226.75:8883","connect.mqtt.service.quality":"2","connect.mqtt.kcql":"INSERT INTO kafka-energy SELECT * FROM LogData WITHCONVERTER=`com.datamountaineer.streamreactor.connect.converters.source.JsonSimpleConverter` WITHKEY(device_id)","connect.progress.enabled":"true","name":"mqtt-source"},"tasks":[],"type":null}

```


使用下面的生成 LogData.java 类:
```bash
java -jar libs/avro-tools-1.8.2.jar compile schema connector/src/main/resources/avro/com/me/mqtt/message/LogData.avsc connector/src/main/groovy
```

## connector

### 概述
connector 可以分为两块：
1. source connector: 利用 Kafka  

处理 IOT 数据，利用 [Kafka-Connector](https://www.confluent.io/product/connectors/)
将数据写入到 [Kafka](https://kafka.apache.org/) 中