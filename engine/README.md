## 概述
fusion engine 从 Kafka 消费数据，利用 [Kafka Stream](https://docs.confluent.io/current/streams/index.html)
处理数据，包括数据插值，融合（从计量实体得到管理实体），聚合（将每十五分钟的数据聚合成逐时，逐日，逐月和逐年）


## Entity
`ManagementEntity` 表示一条管理实体的数据。




## 设计
管理实体转发到下游节点的时候，使用管理实体 id 作为 key，保证同一管理实体的数据都在同一个分区。

## TODO

需要对 ManagementEntity 做一个封装，表示
不存储