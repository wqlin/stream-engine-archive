package com.me.stream.processor.utils

import com.me.entity.LogData
import com.me.entity.ManagementEntity
import com.me.stream.processor.aggregation.MEAggregationProcessor
import com.me.stream.processor.fusion.FusionProcessor
import com.me.stream.processor.fusion.FusionUtils
import com.me.stream.processor.interpolation.InterpolationProcessor
import org.apache.kafka.streams.processor.Processor
import org.apache.kafka.streams.processor.ProcessorSupplier

/**
 * Created by wqlin on 18-4-9 19:06.
 */
public class ProcessorSuppliers {
    private static final String INTERPOLATION_PROCESSOR_NAME = "InterpolationProcessor"

    private static final String MANAGEMENT_ENTITY_AGGREGATION_PROCESSOR_NAME = "ManagementEntityAggregationProcessor"

    private static final String FUSION_PROCESSOR_NAME = "FusionProcessor"


    static class InterpolationProcessorSupplier implements ProcessorSupplier<String, LogData> {
        @Override
        public Processor<String, LogData> get() {
            return new InterpolationProcessor(INTERPOLATION_PROCESSOR_NAME)
        }
    }

    static class FusionProcessorSupplier implements ProcessorSupplier<String, LogData> {
        @Override
        public Processor<String, LogData> get() {
            return new FusionProcessor(FUSION_PROCESSOR_NAME, FusionUtils.createMEManagers())
        }
    }

    static class MEAggregationProcessorSupplier implements ProcessorSupplier<String, ManagementEntity> {
        @Override
        public Processor<String, ManagementEntity> get() {
            return new MEAggregationProcessor(MANAGEMENT_ENTITY_AGGREGATION_PROCESSOR_NAME)
        }
    }
}
