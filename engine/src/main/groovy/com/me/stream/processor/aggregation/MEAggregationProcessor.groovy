package com.me.stream.processor.aggregation

import com.me.entity.ManagementEntity
import org.apache.kafka.streams.state.KeyValueStore
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

import java.util.function.BiFunction

import static com.me.utils.Utils.*

/**
 * Created by wqlin on 18-4-15 10:25.
 * 管理实体(management entity)聚合
 */
public class MEAggregationProcessor extends BasicAggregationProcessor<ManagementEntity> {
    private static final Logger hourLogger = LogManager.getLogger("HourAggregationLogger")
    private static final Logger dayLogger = LogManager.getLogger("DayAggregationLogger")
    private static final Logger monthLogger = LogManager.getLogger("MonthAggregationLogger")
    private static final Logger yearLogger = LogManager.getLogger("YearAggregationLogger")

    public MEAggregationProcessor(String processorName) {
        super(processorName)
        hourLogger.info("[初始化] 创建: " + processorName)
        dayLogger.info("[初始化] 创建: " + processorName)
        monthLogger.info("[初始化] 创建: " + processorName)
        yearLogger.info("[初始化] 创建: " + processorName)
    }

    /**
     * 以聚合逐时数据为例:
     * 比如要聚合 15:00 这一小时的能耗数据, 那么只有收到了 16:00 整点的数据, 才认为聚合成功
     * 在这个过程, 我们会依次收到 15:00, 15:15, 15:30, 15:45 的记录
     * 每收到一条记录, 我们需要进行一次能耗数值的累加, 然后将记录输出给下游节点
     * 这样下游节点就保存了能耗记录的 changelog
     * 但是 15:00 这一记录的时间戳仍然保持为 15:00
     * 使用 last_update 记录上一次记录的时间戳
     * 逐日, 逐月和逐年同理
     * @param dataStore
     * @param forwardRule
     * @param key
     * @param current
     * @param childName
     */
    void forward(KeyValueStore<String, ManagementEntity> dataStore,
                 BiFunction<Long, Long, Boolean> forwardRule,
                 String key,
                 ManagementEntity current,
                 String childName) {
        ManagementEntity previous = dataStore.get(key)
        if (previous != null) {
            long previousUpdateTimestamp = previous.getLastUpdate()
            long currentTimestamp = current.getTimestamp()
            if (previousUpdateTimestamp < currentTimestamp) { // 防止收到冗余记录
                if (forwardRule.apply(previousUpdateTimestamp, currentTimestamp))
                    dataStore.put(key, current)
                else {  // 还没有融合完成
                    double value = round(previous.getValue() + current.getValue(), 4)
                    previous.setValue(value) // 将 current 的能耗值累加到 previous
                    previous.setLastUpdate(currentTimestamp)
                    dataStore.put(key, previous)
                }
                context.forward(key, previous, childName) // 产生 changelog
                String logMsg = "[新纪录]: " + ME2Str(previous)
                if (childName == HOUR_DATA_SINK_NAME)
                    hourLogger.info(logMsg)
                else if (childName == DAY_DATA_SINK_NAME)
                    dayLogger.info(logMsg)
                else if (childName == MONTH_DATA_SINK_NAME)
                    monthLogger.info(logMsg)
                else
                    yearLogger.info(logMsg)
            }
        } else
            dataStore.put(key, current)
    }
}
