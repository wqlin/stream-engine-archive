package com.me.stream.processor.interpolation.strategy

import javax.validation.constraints.NotNull

/**
 * Created by wqlin on 18-4-5 15:14.
 * IOT 数据插值策略抽象接口
 */
interface InterpolationStrategy<K, V> {
    /**
     * @param key 键
     * @param previous 上一次收到的值
     * @param current 这一次收到的值
     * @return 一系列需要恢复的值, 注意结果中不包含 previous 和 current; 如果不需要插值, 那么返回空列表
     */
    @NotNull
    List<V> interpolate(K key, V previous, V current)
}
