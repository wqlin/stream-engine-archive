package com.me.stream.processor.fusion

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody

import java.lang.reflect.Type

/**
 * Created by wqlin on 18-4-13 15:32.
 */
class FusionUtils {
    private static final String URL = "http://122.152.224.169:8086/meta/getFusionInfo"
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8")
    private static final Type RESPONSE_TYPE = new TypeToken<Map<String, Object>>() {}.getType()

    static MEManagers createMEManagers() {
        // TODO update info at remote server
        List<MEManager> MEManagerList
//        try {
//            MEManagerList = getMEManagersFrom(URL)
//            if (MEManagerList.isEmpty())
//                MEManagerList = mockMEManagers()
//        } catch (Exception e) {
//            MEManagerList = mockMEManagers()
//        }
        MEManagerList = mockMEManagers()
        MEManagers managers = new MEManagers(MEManagerList)
        return managers
    }

    static List<MEManager> getMEManagersFrom(String URL) {
        OkHttpClient client = new OkHttpClient()
        RequestBody requestBody = RequestBody.create(JSON, "{\"offset\" : 0,\"size\" : 100}")
        Request request = new Request.Builder()
                .url(URL)
                .post(requestBody)
                .build()

        String responseBody = client.newCall(request).execute().body().string()

        Gson gson = new Gson()
        Map<String, Object> res = gson.fromJson(responseBody, RESPONSE_TYPE)
        Map<String, Object> data = (Map<String, Object>) ((Map<String, Object>) res.get("response")).get("data")

        List<MEManager> MEManagerList = new ArrayList<>()
        for (Map.Entry<String, Object> entry : data) {
            String entityId = entry.key
            List<Map<String, Object>> relationships = (List<Map<String, Object>>) entry.value
            Map<String, Double> mapping = new HashMap<>()
            for (Map<String, Object> relationship : relationships) {
                String deviceId = (String) relationship.get("device_id")
                Double factor = (Double) relationship.get("factor")
                mapping.put(deviceId, factor)
            }
            MEManager manager = new MEManager(entityId, mapping)
            MEManagerList.add(manager)
        }

        return MEManagerList
    }

    private static List<MEManager> mockMEManagers() {
        List<MEManager> MEManagerList = new ArrayList<>()

        Map<String, Double> r1 = new HashMap<>()
        r1.put("A6B525254CCD45049315D5CD9FFBF492", 1.0)
        MEManager m1 = new MEManager("0001", r1)
        MEManagerList.add(m1)

        Map<String, Double> r2 = new HashMap<>()
        r2.put("F4E4BE9E51F24E71BE3B140C2E20B0B0", 1.0)
        r2.put("A6B525254CCD45049315D5CD9FFBF492", 1.0)
        MEManager m2 = new MEManager("0004", r2)
        MEManagerList.add(m2)

        Map<String, Double> r3 = new HashMap<>()
        r3.put("E267B516E353445F8F044321DBAF1AA6", 1.0)
        r3.put("A6B525254CCD45049315D5CD9FFBF492", 1.0)
        r3.put("F4E4BE9E51F24E71BE3B140C2E20B0B0", 1.0)
        MEManager m3 = new MEManager("0006", r3)
        MEManagerList.add(m3)

        return MEManagerList
    }
}
