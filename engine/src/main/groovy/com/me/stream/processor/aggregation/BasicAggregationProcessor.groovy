package com.me.stream.processor.aggregation

import org.apache.kafka.streams.processor.Processor
import org.apache.kafka.streams.processor.ProcessorContext
import org.apache.kafka.streams.state.KeyValueStore

import static com.me.stream.processor.aggregation.AggregationRules.*
import java.util.function.BiFunction

/**
 * Created by wqlin on 18-4-9 20:29.
 * 将管理实体每十五分钟的数据聚合成逐时，逐天，逐月和逐年
 */
public abstract class BasicAggregationProcessor<V> implements Processor<String, V> {
    public static final String HOUR_DATA_SINK_NAME = "HourDataSink"
    public static final String DAY_DATA_SINK_NAME = "DayDataSink"
    public static final String MONTH_DATA_SINK_NAME = "MonthDataSink"
    public static final String YEAR_DATA_SINK_NAME = "YearDataSink"

    protected ProcessorContext context

    private String processorName

    private KeyValueStore<String, V> hourDataStore // 逐时数据
    private KeyValueStore<String, V> dayDataStore // 逐日数据
    private KeyValueStore<String, V> monthDataStore // 逐月数据
    private KeyValueStore<String, V> yearDataStore // 逐年数据

    BasicAggregationProcessor(String processorName) {
        this.processorName = processorName
    }

    @Override
    @SuppressWarnings("unchecked")
    public void init(ProcessorContext context) {
        this.context = context
        hourDataStore = (KeyValueStore) context.getStateStore("hourDataStore");
        dayDataStore = (KeyValueStore) context.getStateStore("dayDataStore");
        monthDataStore = (KeyValueStore) context.getStateStore("monthDataStore");
        yearDataStore = (KeyValueStore) context.getStateStore("yearDataStore");
    }

    // 生成 changelog 并 forward 给下游节点
    // changelog 定义: https://docs.confluent.io/current/streams/concepts.html#duality-of-streams-and-tables
    abstract void forward(KeyValueStore<String, V> dataStore,
                          BiFunction<Long, Long, Boolean> forwardRule,
                          String key,
                          V current,
                          String childName)

    @Override
    public void process(String key, V current) {
        forward(hourDataStore, hourAggregationRule, key, current, HOUR_DATA_SINK_NAME) // 判断逐时数据是否聚合完成, 下面类似
        forward(dayDataStore, dayAggregationRule, key, current, DAY_DATA_SINK_NAME)
        forward(monthDataStore, monthAggregationRule, key, current, MONTH_DATA_SINK_NAME)
        forward(yearDataStore, yearAggregationRule, key, current, YEAR_DATA_SINK_NAME)
        context.commit()
    }

    @Override
    @Deprecated
    public void punctuate(long timestamp) {
    }

    @Override
    public void close() {
    }
}
