package com.me.stream.processor.fusion

import com.me.entity.LogData
import com.me.entity.ManagementEntity
import com.me.entity.ManagementEntityGenerator
import com.me.entity.ManagementEntityWrapper
import org.apache.kafka.streams.processor.Processor
import org.apache.kafka.streams.processor.ProcessorContext
import org.apache.kafka.streams.state.KeyValueStore
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

import java.util.concurrent.atomic.AtomicInteger

import static com.me.utils.Utils.*

/**
 * Created by wqlin on 18-4-13 09:59.
 * 将计算实体融合成管理实体, 一个管理实体由一个或者多个计算实体组成
 */
public class FusionProcessor implements Processor<String, LogData> {
    private static final Logger processorLogger = LogManager.getLogger("FusionProcessorLogger")
    private static final Logger fusionLogger = LogManager.getLogger("FusionLogger")

    public static final int PENDING_ENTITIES_MAX_LENGTH = 10000

    private ProcessorContext context

    private String processorName

    private KeyValueStore<String, ManagementEntityGenerator> dataStore

    private static final AtomicInteger counter = new AtomicInteger(1)

    private MEManagers managers

    private boolean initialized

    FusionProcessor(String name, MEManagers managers) {
        this.processorName = name + "-" + String.valueOf(counter.getAndIncrement())
        this.managers = managers
        initialized = false
        processorLogger.info("[初始化]: 创建 " + processorName)
        processorLogger.info("[初始化]: MEManagers " + managers)
    }

    private void notifyNewValue(final ManagementEntityGenerator entityGenerator,
                                final MEManager manager,
                                final String deviceId,
                                double value,
                                long timestamp) {
        final String entityId = manager.getEntityId()
        List<ManagementEntityWrapper> pendingEntities = entityGenerator.getPendingManagementEntities()
        //找到具有相同 timestamp 的 ManagementEntityWrapper
        Optional<Tuple2<ManagementEntityWrapper, Integer>> optionalWrapper = pendingEntities
                .withIndex()
                .stream()
                .filter { it -> it.first.getEntity().getTimestamp() == timestamp }
                .findFirst()

        if (optionalWrapper.isPresent()) {
            ManagementEntityWrapper pendingEntityWrapper = optionalWrapper.get().first
            ManagementEntity pendingEntity = pendingEntityWrapper.getEntity()
            int index = optionalWrapper.get().second
            Double factor = manager.getFactorFor(deviceId)

            int pendingCount = pendingEntityWrapper.getCount()
            if (factor != null) {
                pendingEntity.setValue(pendingEntity.getValue() + factor * value)
                pendingCount -= 1
            }

            if (pendingCount == 0) { // 融合新纪录
                processorLogger.info("entityId: " + entityId + " pendingEntities size: " + pendingEntities.size())
                pendingEntities.remove(index)
                processorLogger.info("After remove entityId: " + entityId + " pendingEntities size: " + pendingEntities.size())

                context.forward(entityId, pendingEntity)
                fusionLogger.info("[融合新纪录], 分区: " + context.partition() + ", 记录: " + ME2Str(pendingEntity))
            } else {
                pendingEntityWrapper.setCount(pendingCount)
                pendingEntities.set(index, pendingEntityWrapper)
                processorLogger.info("实体记录: " + ME2Str(pendingEntity) + " 剩余需融合次数: " + pendingCount)
            }
        } else { // 新纪录
            ManagementEntity newEntity = ManagementEntity
                    .newBuilder()
                    .setEntityId(entityId)
                    .setValue(value)
                    .setTimestamp(timestamp)
                    .setLastUpdate(timestamp)
                    .build()
            int remainingFusionCount = manager.getTotalDeviceNumber() - 1
            if (remainingFusionCount == 0) {
                context.forward(entityId, newEntity)
                fusionLogger.info("[融合新纪录], 分区: " + context.partition() + ", 记录: " + ME2Str(newEntity))
            } else {
                pendingEntities.add(new ManagementEntityWrapper(newEntity, remainingFusionCount))
                processorLogger.info(processorName + " 加入新纪录 " + ME2Str(newEntity) + " 设备 ID: " + deviceId + " pendingEntities 大小: " + pendingEntities.size())
            }
        }
        while (pendingEntities.size() > PENDING_ENTITIES_MAX_LENGTH) {
            ManagementEntityWrapper entityWrapper = pendingEntities.remove(0)
            processorLogger.error("entityId: " + entityId + " 的待融合队列太大, 删除待适合实体: " + ME2Str(entityWrapper.getEntity()))
        }
        entityGenerator.setPendingManagementEntities(pendingEntities)
        try {
            dataStore.put(entityId, entityGenerator)
        } catch (Exception e) {
            processorLogger.error(e)
        }

    }

    @Override
    @SuppressWarnings("unchecked")
    public void init(ProcessorContext context) {
        this.context = context
        dataStore = (KeyValueStore) this.context.getStateStore("MEDataStore")
        processorLogger.info(processorName + " 初始完成")
    }

    private void initialize() {
        for (MEManager manager : managers.getManagers()) {
            String entityId = manager.getEntityId()
            if (dataStore.get(entityId) == null) { // 当对应的管理实体生成器不存在才创建
                ManagementEntityGenerator entityGenerator = ManagementEntityGenerator
                        .newBuilder()
                        .setPendingManagementEntities(new LinkedList<ManagementEntityWrapper>())
                        .build()
                dataStore.put(entityId, entityGenerator)
                processorLogger.info(processorName + " 加入新的管理实体, id: " + entityId + " mapping: " + manager.getRelationship())
            } else
                processorLogger.info(processorName + " 存在管理实体, id: " + entityId + " mapping: " + manager.getRelationship())
        }
    }

    @Override
    public void process(String key, LogData current) {
        if (!initialized) {
            initialize()
            initialized = true
        }
        List<MEManager> managerList = managers.findManagerContains(current.getDeviceId())
        for (MEManager manager : managerList) {
            ManagementEntityGenerator entityGenerator = dataStore.get(manager.getEntityId())
            if (entityGenerator != null)
                notifyNewValue(entityGenerator, manager, current.getDeviceId(), current.getValue(), current.getPeriodicAt())
            else
                processorLogger.error("DataStore 找不到包含 " + manager.getEntityId() + " 的 ManagementEntityGenerator")
        }
        context.commit()
    }

    @Override
    @Deprecated
    public void punctuate(long timestamp) {
    }

    @Override
    public void close() {
    }
}
