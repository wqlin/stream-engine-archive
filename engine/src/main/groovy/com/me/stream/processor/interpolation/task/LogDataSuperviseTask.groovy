package com.me.stream.processor.interpolation.task

import com.me.entity.LogData
import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.state.KeyValueIterator
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

import java.util.concurrent.TimeUnit

import static com.me.stream.processor.interpolation.task.TaskUtils.*
import static com.me.utils.Utils.*

/**
 * Created by wqlin on 18-3-29 10:27.
 * 定时读取插值处理器流应用状态, 如果有太长时间没有更新, 那么需要告警
 */
class LogDataSuperviseTask implements Runnable {
    private static final Logger logger = LogManager.getLogger("LogDataSuperviseTaskLogger")

    private ReadOnlyKeyValueStore<String, LogData> dataStore

    private LogDataSuperviseTask() {
    }

    LogDataSuperviseTask(ReadOnlyKeyValueStore<String, LogData> dataStore) {
        this.dataStore = dataStore
        logger.info("[初始化] 创建插值监控任务...")
    }

    @Override
    void run() {
        KeyValueIterator<String, LogData> iterators = dataStore.all()
        long currentMillis = System.currentTimeMillis()
        logger.info("执行定时任务: " + milliSecond2DateStr(currentMillis))
        for (KeyValue<String, LogData> iterator : iterators) {
            String key = iterator.key
            LogData value = iterator.value
            long periodic_at = value.getPeriodicAt()
            if (currentMillis - periodic_at > MAXIMUM_TIME_DIFFERENCE)
                logger.warn("设备: " + key + " 没有更新, 最近一次记录为: " + LogData2Str(value))
        }
    }
}
