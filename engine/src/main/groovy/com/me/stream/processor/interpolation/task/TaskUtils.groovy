package com.me.stream.processor.interpolation.task

import java.util.concurrent.TimeUnit

/**
 * Created by wqlin on 18-4-5 13:54.
 */
class TaskUtils {
    // 定时任务执行间隔
    static final int QUERY_TASK_PERIOD_IN_MINUTE = 60

    // 定时任务执行时, 允许记录与现在时间的最大时间差
    static final long MAXIMUM_TIME_DIFFERENCE = TimeUnit.HOURS.toMillis(1)
}
