package com.me.stream.processor

import com.me.Constants
import com.me.entity.LogData
import com.me.entity.ManagementEntity
import com.me.stream.processor.aggregation.BasicAggregationProcessor
import com.me.stream.processor.utils.ProcessorSuppliers
import com.me.stream.processor.utils.ProcessorUtils
import com.me.stream.processor.utils.PropertyUtils
import io.confluent.kafka.serializers.KafkaAvroDeserializer
import io.confluent.kafka.serializers.KafkaAvroSerializer
import io.confluent.kafka.streams.serdes.avro.SpecificAvroDeserializer
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerializer
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.state.Stores

/**
 * Created by wqlin on 18-4-21 14:28.
 */
class Processors {
    public static final KafkaStreams createInterpolationProcessor() {
        Topology builder = new Topology();
        builder.addSource("Source", ProcessorUtils.createKeyDeserializer(), ProcessorUtils.createLogDataDeserializer(), Constants.RAW_DATA_TOPIC)
        builder.addProcessor("InterpolationProcessor", new ProcessorSuppliers.InterpolationProcessorSupplier(), "Source");

        builder.addStateStore(Stores.keyValueStoreBuilder(
                Stores.persistentKeyValueStore("rawDataStore"),
                Serdes.String(),
                ProcessorUtils.createLogdataSerde()),
                "InterpolationProcessor");

        builder.addSink("Sink", Constants.MATURE_DATA_TOPIC, ProcessorUtils.createKeySerializer(), ProcessorUtils.createLogDataSerializer(), "InterpolationProcessor");

        Properties props = PropertyUtils.createBasicStreamProperties("interpolation-processor", "interpolation-processor-client")
        final KafkaStreams stream = new KafkaStreams(builder, props)
        return stream
    }

    public static final KafkaStreams createFusionProcessor() {
        Topology builder = new Topology()
        builder.addSource("Source", ProcessorUtils.createKeyDeserializer(), ProcessorUtils.createLogDataDeserializer(), Constants.MATURE_DATA_TOPIC)
        builder.addProcessor("FusionProcessor", new ProcessorSuppliers.FusionProcessorSupplier(), "Source");

        builder.addStateStore(Stores.keyValueStoreBuilder(
                Stores.persistentKeyValueStore("MEDataStore"),
                Serdes.String(),
                ProcessorUtils.createMEGeneratorSerde()),
                "FusionProcessor")

        builder.addSink("Sink", Constants.ME_MINUTE_DATA_TOPIC, ProcessorUtils.createKeySerializer(), ProcessorUtils.createMESerializer(), "FusionProcessor");

        Properties props = PropertyUtils.createBasicStreamProperties("fusion-processor", "fusion-processor-client")
        final KafkaStreams stream = new KafkaStreams(builder, props)
        return stream
    }


    public static final KafkaStreams createAggregationProcessor() {
        Topology builder = new Topology()
        builder.addSource("Source", ProcessorUtils.createKeyDeserializer(), ProcessorUtils.createMEDeserializer(), Constants.ME_MINUTE_DATA_TOPIC)
        builder.addProcessor("AggregationProcessor", new ProcessorSuppliers.MEAggregationProcessorSupplier(), "Source");

        Serde<ManagementEntity> MESerde = ProcessorUtils.createMESerde()
        builder.addStateStore(Stores.keyValueStoreBuilder(
                Stores.persistentKeyValueStore("hourDataStore"),
                Serdes.String(),
                MESerde),
                "AggregationProcessor");

        builder.addStateStore(Stores.keyValueStoreBuilder(
                Stores.persistentKeyValueStore("dayDataStore"),
                Serdes.String(),
                MESerde),
                "AggregationProcessor")

        builder.addStateStore(Stores.keyValueStoreBuilder(
                Stores.persistentKeyValueStore("monthDataStore"),
                Serdes.String(),
                MESerde),
                "AggregationProcessor");

        builder.addStateStore(Stores.keyValueStoreBuilder(
                Stores.persistentKeyValueStore("yearDataStore"),
                Serdes.String(),
                MESerde),
                "AggregationProcessor");

        KafkaAvroSerializer sinkKeySerializer = ProcessorUtils.createKeySerializer()
        SpecificAvroSerializer<ManagementEntity> sinkValueSerializer = ProcessorUtils.createMESerializer()

        builder.addSink(BasicAggregationProcessor.HOUR_DATA_SINK_NAME, Constants.ME_HOUR_DATA_TOPIC, sinkKeySerializer, sinkValueSerializer, "AggregationProcessor");
        builder.addSink(BasicAggregationProcessor.DAY_DATA_SINK_NAME, Constants.ME_DAY_DATA_TOPIC, sinkKeySerializer, sinkValueSerializer, "AggregationProcessor");
        builder.addSink(BasicAggregationProcessor.MONTH_DATA_SINK_NAME, Constants.ME_MONTH_DATA_TOPIC, sinkKeySerializer, sinkValueSerializer, "AggregationProcessor");
        builder.addSink(BasicAggregationProcessor.YEAR_DATA_SINK_NAME, Constants.ME_YEAR_DATA_TOPIC, sinkKeySerializer, sinkValueSerializer, "AggregationProcessor");

        Properties props = PropertyUtils.createBasicStreamProperties("aggregation-processor", "aggregation-processor-client")
        final KafkaStreams stream = new KafkaStreams(builder, props)
        return stream
    }
}
