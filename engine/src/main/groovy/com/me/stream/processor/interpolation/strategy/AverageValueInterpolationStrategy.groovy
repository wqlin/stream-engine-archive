package com.me.stream.processor.interpolation.strategy

import com.me.entity.LogData
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import static com.me.utils.Utils.*

import java.util.concurrent.TimeUnit

/**
 * Created by wqlin on 18-4-5 15:16.
 * LogData 类的平均值插值实现
 */
class AverageValueInterpolationStrategy implements InterpolationStrategy<String, LogData> {
    private static final Logger interpolationLogger = LogManager.getLogger("AverageValueInterpolationStrategyLogger")
    private static final long FIFTEEN_MINUTE_IN_MILLIS = TimeUnit.MINUTES.toMillis(15)
    private static final long DEVIATION_MILLIS = 0 //TimeUnit.SECONDS.toMillis(1)

    // 同一设备连续记录的最大时间差, 允许误差
    private static final long CONTINUOUS_RECORD_MAXIMUM_TIME_DIFFERENCE = FIFTEEN_MINUTE_IN_MILLIS + DEVIATION_MILLIS

    static {
        interpolationLogger.info("同一设备连续记录的最大时间差毫秒数: " + CONTINUOUS_RECORD_MAXIMUM_TIME_DIFFERENCE)
    }

    private static long getTimeDifferenceInMillis(long beforeTime, long afterTime) {
        return afterTime - beforeTime
    }

    // 使用平均值进行插值
    List<LogData> interpolate(String key, LogData previous, LogData current) {
        long previousPeriodicAt = previous.getPeriodicAt(), currentPeriodicAt = current.getPeriodicAt()
        long timeDifference = getTimeDifferenceInMillis(previousPeriodicAt, currentPeriodicAt)
        List<LogData> missingDataList = new ArrayList<>()
        if (timeDifference > CONTINUOUS_RECORD_MAXIMUM_TIME_DIFFERENCE) {// 需要插值
            int count = (int) Math.round(timeDifference / CONTINUOUS_RECORD_MAXIMUM_TIME_DIFFERENCE)
            double currentAccValue = current.getAccValue(), previousAccValue = previous.getAccValue()
            double averageAccValue = (currentAccValue - previousAccValue) / count
            interpolationLogger.warn("Key: " + key + " 需要插值, 上一次记录时间戳: " + milliSecond2DateStr(previousPeriodicAt) + " 本次记录时间戳: " + milliSecond2DateStr(currentPeriodicAt) +
                    " 需插值个数: " + (count - 1) + ", 上一次记录能耗累积读数: " + String.format("%.4f", previousAccValue) + ", 本次记录能耗累积读数: " + String.format("%.4f", currentAccValue)
                    + ", 插值平均能耗值: " + String.format("%.4f", averageAccValue))
            double acc = previousAccValue
            for (int i = 1; i < count; i++) {
                acc += averageAccValue
                LogData data = LogData
                        .newBuilder()
                        .setUuid("")
                        .setDeviceId(current.getDeviceId())
                        .setLogAddr(current.getLogAddr())
                        .setAccValue(round(acc, 4))
                        .setValue(0.0)
                        .setMinValue(current.getMinValue())
                        .setMaxValue(current.getMaxValue())
                        .setExceed(current.getExceed())
                        .setPeriodicAt(previousPeriodicAt + i * FIFTEEN_MINUTE_IN_MILLIS)
                        .setSource(1) // 插值得到
                        .build()
                missingDataList.add(data)
            }
        }
        return missingDataList
    }
}
