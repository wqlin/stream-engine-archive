package com.me.stream.processor.interpolation

import com.me.entity.LogData
import com.me.stream.processor.interpolation.strategy.AverageValueInterpolationStrategy
import com.me.stream.processor.interpolation.strategy.InterpolationStrategy

import java.util.concurrent.atomic.AtomicInteger

import static com.me.utils.Utils.*

import org.apache.kafka.streams.processor.Processor
import org.apache.kafka.streams.processor.ProcessorContext
import org.apache.kafka.streams.state.KeyValueStore
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

/**
 * Created by wqlin on 18-4-5 20:03.
 * 插值填充, https://en.wikipedia.org/wiki/Interpolation
 */
class InterpolationProcessor implements Processor<String, LogData> {
    private static final Logger logger = LogManager.getLogger("InterpolationProcessorLogger")

    private InterpolationStrategy interpolationStrategy

    private ProcessorContext context

    /**
     * Key: 设备 ID
     * Value: 该设备 ID 最近一次收到的采集数据记录
     */
    private KeyValueStore<String, LogData> dataStore

    private static final AtomicInteger counter = new AtomicInteger(1)

    private String processorName

    public InterpolationProcessor(String name) {
        this.processorName = name + "-" + String.valueOf(counter.getAndIncrement())
        logger.info("[初始化]: 创建 " + processorName)
    }

    @Override
    @SuppressWarnings("unchecked")
    public void init(ProcessorContext context) {
        this.interpolationStrategy = new AverageValueInterpolationStrategy()
        this.context = context
        dataStore = (KeyValueStore) context.getStateStore("rawDataStore");
        logger.info("[初始化]: " + processorName + " 初始完成")
    }

    private void forward(String key, LogData record) {
        double round_value = round(record.getValue(), 4)
        record.setValue(round_value)
        double round_acc_value = round(record.getAccValue(), 4)
        record.setAccValue(round_acc_value)
        logger.info("[新纪录]: 分区: " + context.partition() + " " + LogData2Str(record))
        context.forward(key, record)
    }

    /**
     * record-at-a-time 处理, 使用 InterpolationStrategy 进行插值处理
     * @param key 设备 ID
     * @param current
     */
    @Override
    public void process(String key, LogData current) {
        LogData previous = dataStore.get(key)
        if (previous != null && previous.getPeriodicAt() < current.getPeriodicAt()) { // 通过对比前后记录的时间戳判断是否是冗余记录
            if (previous.getAccValue() > current.getAccValue()) {
                logger.error("[错误]: 键: " + key + " 在 " + milliSecond2DateStr(current.getPeriodicAt()) +
                        " 的累计读数: " + current.getAccValue() + " 小于其在: " + milliSecond2DateStr(previous.getPeriodicAt())
                        + " 的累计读数: " + previous.getAccValue())
            } else {
                List<LogData> missingDataList = interpolationStrategy.interpolate(key, previous, current)
                double acc_value = previous.getAccValue()
                for (LogData missingData : missingDataList) {
                    missingData.setValue(missingData.getAccValue() - acc_value) // 计算能耗数值
                    acc_value = missingData.getAccValue()
                    forward(key, missingData)  // 发给下游
                }
                current.setValue(current.getAccValue() - acc_value)
                forward(key, current)
                dataStore.put(key, current)
            }
        } else if (previous == null) {
            current.setValue(0)
            forward(key, current)
            dataStore.put(key, current)
        }
        context.commit()
    }

    @Override
    @Deprecated
    public void punctuate(long timestamp) {
    }

    @Override
    public void close() {
    }
}