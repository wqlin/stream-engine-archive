package com.me.stream.processor.fusion

/**
 * Created by wqlin on 18-4-24 10:30.
 * 管理实体 id 和组成实体的设备和 factor
 */
public class MEManager {
    /**
     * 管理实体 ID
     */
    private String entityId

    /**
     * 设备 ID 与计算关系
     */
    private Map<String, Double> relationship

    public MEManager(String entityId, Map<String, Double> relationship) {
        this.entityId = entityId
        this.relationship = relationship
    }

    public boolean containsDevice(String deviceId) {
        return relationship.containsKey(deviceId)
    }

    public String getEntityId() {
        return entityId
    }

    public Double getFactorFor(String deviceId) {
        return relationship.getOrDefault(deviceId, 0.0)
    }

    public Map<String, Double> getRelationship() {
        return relationship
    }

    public int getTotalDeviceNumber() {
        return relationship.size()
    }

    public String toString() {
        return "实体 ID: " + entityId +
                ", 映射关系: " + relationship
    }
}