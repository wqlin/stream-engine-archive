package com.me.stream.processor.utils

import com.me.Constants
import com.me.entity.LogData
import com.me.entity.ManagementEntity
import com.me.entity.ManagementEntityGenerator
import io.confluent.kafka.serializers.KafkaAvroDeserializer
import io.confluent.kafka.serializers.KafkaAvroSerializer
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig
import io.confluent.kafka.streams.serdes.avro.SpecificAvroDeserializer
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerializer
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.errors.InvalidStateStoreException
import org.apache.kafka.streams.state.QueryableStoreType
import sun.rmi.runtime.Log

import java.math.RoundingMode
import java.security.acl.LastOwnerException
import java.util.function.BiFunction

/**
 * Created by wqlin on 18-4-9 19:06.
 */
class ProcessorUtils {
    public static <T> T waitUntilStoreIsQueryable(final String storeName,
                                                  final QueryableStoreType<T> queryableStoreType,
                                                  final KafkaStreams streams) {
        while (true) {
            try {
                return streams.store(storeName, queryableStoreType);
            } catch (InterruptedException e) {
                return null;
            } catch (InvalidStateStoreException | IllegalStateException ignored) {
                // store not yet ready for querying
                Thread.sleep(1000);
            }
        }
    }

    public static final KafkaAvroSerializer createKeySerializer() {
        KafkaAvroSerializer keySerializer = new KafkaAvroSerializer()
        Map<String, String> config = PropertyUtils.createSerdeConfig()
        keySerializer.configure(config, true)
        return keySerializer
    }

    public static final KafkaAvroDeserializer createKeyDeserializer() {
        KafkaAvroDeserializer keyDeserializer = new KafkaAvroDeserializer()
        Map<String, String> config = PropertyUtils.createSerdeConfig()
        keyDeserializer.configure(config, true)
        return keyDeserializer
    }

    public static final SpecificAvroSerializer<LogData> createLogDataSerializer() {
        SpecificAvroSerializer<LogData> LogDataSerializer = new SpecificAvroSerializer<>()
        Map<String, String> config = PropertyUtils.createSerdeConfig()
        LogDataSerializer.configure(config, false)
        return LogDataSerializer
    }

    public static final SpecificAvroDeserializer<LogData> createLogDataDeserializer() {
        SpecificAvroDeserializer<LogData> LogDataDeserializer = new SpecificAvroDeserializer<>()
        Map<String, String> config = PropertyUtils.createSerdeConfig()
        LogDataDeserializer.configure(config, false)
        return LogDataDeserializer
    }

    public static final SpecificAvroSerializer<ManagementEntity> createMESerializer() {
        SpecificAvroSerializer<ManagementEntity> MESerializer = new SpecificAvroSerializer<>()
        Map<String, String> config = PropertyUtils.createSerdeConfig()
        MESerializer.configure(config, false)
        return MESerializer
    }

    public static final SpecificAvroDeserializer<ManagementEntity> createMEDeserializer() {
        SpecificAvroDeserializer<ManagementEntity> MEDeserializer = new SpecificAvroDeserializer<>()
        Map<String, String> config = PropertyUtils.createSerdeConfig()
        MEDeserializer.configure(config, false)
        return MEDeserializer
    }

    public static Serde<LogData> createLogdataSerde() {
        Serde<LogData> LogDataSerde = new SpecificAvroSerde<>()
        Map<String, String> LogDataSerdeConfig = new HashMap<>();
        LogDataSerdeConfig.put("schema.registry.url", Constants.SCHEMA_REGISTRY_URL);
        LogDataSerde.configure(LogDataSerdeConfig, false)
        return LogDataSerde
    }

    public static Serde<ManagementEntity> createMESerde() {
        Serde<ManagementEntity> MESerde = new SpecificAvroSerde<>()
        Map<String, String> MESerdeConfig = new HashMap<>()
        MESerdeConfig.put("schema.registry.url", Constants.SCHEMA_REGISTRY_URL)
        MESerdeConfig.put(KafkaAvroSerializerConfig.AUTO_REGISTER_SCHEMAS, "true");
        MESerde.configure(MESerdeConfig, false)
        return MESerde
    }

    public static final Serde<ManagementEntityGenerator> createMEGeneratorSerde() {
        Serde<ManagementEntityGenerator> MEGeneratorSerde = new SpecificAvroSerde<>()
        Map<String, String> MEConfig = new HashMap<>()
        MEConfig.put("schema.registry.url", Constants.SCHEMA_REGISTRY_URL)
        MEConfig.put(KafkaAvroSerializerConfig.AUTO_REGISTER_SCHEMAS, "true");
        MEGeneratorSerde.configure(MEConfig, false);
        return MEGeneratorSerde
    }
}
