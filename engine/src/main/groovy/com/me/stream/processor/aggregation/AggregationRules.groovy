package com.me.stream.processor.aggregation

import java.util.function.BiFunction

/**
 * Created by wqlin on 18-4-12 15:50.
 */
class AggregationRules {

    static abstract class AggregationRule implements BiFunction<Long, Long, Boolean> {
        protected Calendar calendar = Calendar.getInstance()

        int getYear(long t) {
            calendar.setTimeInMillis(t)
            return calendar.get(Calendar.YEAR)
        }

        int getMonth(long t) {
            calendar.setTimeInMillis(t)
            return calendar.get(Calendar.MONTH)
        }

        int getDayOfYear(long t) {
            calendar.setTimeInMillis(t)
            return calendar.get(Calendar.DAY_OF_YEAR)
        }

        int getHourOfDay(long t) {
            calendar.setTimeInMillis(t)
            return calendar.get(Calendar.HOUR_OF_DAY)
        }

        Boolean apply(Long t1, Long t2) {
            return true
        }
    }


    public static final BiFunction<Long, Long, Boolean> hourAggregationRule =
            new AggregationRule() {
                @Override
                Boolean apply(Long t1, Long t2) {
                    int year1 = getYear(t1)
                    int day1 = getDayOfYear(t1)
                    int hour1 = getHourOfDay(t1)

                    int year2 = getYear(t2)
                    int day2 = getDayOfYear(t2)
                    int hour2 = getHourOfDay(t2)
                    if (year1 == year2) {
                        if (day1 == day2) {
                            return hour2 > hour1
                        } else return true
                    } else return true
                }
            }


    public static final BiFunction<Long, Long, Boolean> dayAggregationRule =
            new AggregationRule() {
                @Override
                Boolean apply(Long t1, Long t2) {
                    int year1 = getYear(t1)
                    int day1 = getDayOfYear(t1)

                    int year2 = getYear(t2)
                    int day2 = getDayOfYear(t2)
                    if (year1 == year2)
                        return day2 > day1
                    else return true
                }
            }


    public static final BiFunction<Long, Long, Boolean> monthAggregationRule =
            new AggregationRule() {
                @Override
                Boolean apply(Long t1, Long t2) {
                    int year1 = getYear(t1)
                    int month1 = getMonth(t1)

                    int year2 = getYear(t2)
                    int month2 = getMonth(t2)
                    if (year1 == year2)
                        return month2 > month1
                    else return true
                }
            }

    public static final BiFunction<Long, Long, Boolean> yearAggregationRule =
            new AggregationRule() {
                @Override
                Boolean apply(Long t1, Long t2) {
                    return getYear(t2) > getYear(t1)
                }
            }
}
