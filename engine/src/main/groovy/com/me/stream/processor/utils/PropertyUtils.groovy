package com.me.stream.processor.utils

import com.me.Constants
import com.me.entity.LogData
import io.confluent.kafka.serializers.KafkaAvroSerializerConfig
import io.confluent.kafka.streams.serdes.avro.SpecificAvroSerde
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.streams.StreamsConfig

/**
 * Created by wqlin on 18-4-18 22:29.
 */
class PropertyUtils {
    /**
     * 创建流应用需要的基本 property
     * @param applicationId
     * @param clientId
     * @param processGuarantee 1: at least once, 2: exactly once
     * @param offset 1: earliest 2: latest
     * @return
     */
    public
    static Properties createBasicStreamProperties(String applicationId, String clientId, int processGuarantee = 1, int offset = 1) throws IllegalArgumentException {
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, applicationId);
        props.put(StreamsConfig.CLIENT_ID_CONFIG, clientId)
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, Constants.BOOTSTRAP_SERVERS_CONFIG)
        props.put("schema.registry.url", Constants.SCHEMA_REGISTRY_URL)
        if (processGuarantee == 1)
            props.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, StreamsConfig.AT_LEAST_ONCE)
        else if (processGuarantee == 2)
            props.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, StreamsConfig.EXACTLY_ONCE)
        else
            throw new IllegalArgumentException("processGuarantee 只能为 1 或者 2")
        if (offset == 1)
            props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        else if (offset == 2) {
            props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest")
        } else
            throw new IllegalArgumentException("offset 只能为 1 或者 2")
        return props
    }

    public static Map<String, String> createSerdeConfig() {
        Map<String, String> config = new HashMap<>()
        config.put("specific.avro.reader", "true")
        config.put("schema.registry.url", Constants.SCHEMA_REGISTRY_URL)
        config.put(KafkaAvroSerializerConfig.AUTO_REGISTER_SCHEMAS, "true")
        return config
    }
}
