package com.me.stream.processor.fusion

import java.util.stream.Collectors

/**
 * Created by wqlin on 18-4-23 15:10.
 */
public class MEManagers {
    private List<MEManager> managers

    public MEManagers(List<MEManager> managers) {
        this.managers = managers
    }

    public List<MEManager> getManagers() {
        return managers
    }

    public List<MEManager> findManagerContains(final String deviceId) {
        return managers.stream().filter { it ->
            it.containsDevice(deviceId)
        }.collect(Collectors.toList())
    }

    public String toString() {
        StringBuilder builder = new StringBuilder()
        builder.append("{\n")
        List<String> strs = managers.stream().map { it -> it.toString() }.collect(Collectors.toList())
        for (String str : strs)
            builder.append("\t" + str + "\n")
        builder.append("\n}")
    }
}
