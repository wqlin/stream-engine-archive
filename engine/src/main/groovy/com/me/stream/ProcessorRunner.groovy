package com.me.stream

import com.me.entity.LogData
import com.me.stream.processor.Processors
import com.me.stream.processor.interpolation.task.LogDataSuperviseTask
import com.me.stream.processor.interpolation.task.TaskUtils
import com.me.stream.processor.utils.ProcessorUtils
import javafx.concurrent.Task
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.state.QueryableStoreTypes
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

import java.util.concurrent.CountDownLatch
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

/**
 * Created by wqlin on 18-4-21 15:19.
 */
class ProcessorRunner {
    private static final Logger logger = LogManager.getLogger("ProcessorRunnerLogger")

    public static void main(String[] args) {
        logger.info("开始...")
        final KafkaStreams interpolationProcessor = Processors.createInterpolationProcessor()
        interpolationProcessor.cleanUp()
        interpolationProcessor.start()
        logger.info("启动插值流处理任务...")

        final KafkaStreams fusionProcessor = Processors.createFusionProcessor()
        fusionProcessor.cleanUp()
        fusionProcessor.start()
        logger.info("启动融合流处理任务...")

        final KafkaStreams aggregationProcessor = Processors.createAggregationProcessor()
        aggregationProcessor.cleanUp()
        aggregationProcessor.start()
        logger.info("启动聚合流处理任务...")

        logger.info("创建插值流处理监控任务...");
        // 创建定时轮询任务
        ReadOnlyKeyValueStore<String, LogData> logDataStore = ProcessorUtils.waitUntilStoreIsQueryable("rawDataStore", QueryableStoreTypes.<String, LogData> keyValueStore(), interpolationProcessor)
        final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1)

        if (logDataStore != null) {
            LogDataSuperviseTask task = new LogDataSuperviseTask(logDataStore);
            // 单线程执行
            scheduler.scheduleAtFixedRate(task, TaskUtils.QUERY_TASK_PERIOD_IN_MINUTE, TaskUtils.QUERY_TASK_PERIOD_IN_MINUTE, TimeUnit.MINUTES)
        }
        logger.info("监控任务创建完成...")
        logger.info("所有流处理任务已启动完成...")

        final CountDownLatch latch = new CountDownLatch(1);
        latch.await()
        Runtime.getRuntime().addShutdownHook(new Thread("processor-shutdown-hook") {
            @Override
            void run() {
                interpolationProcessor.close()
                fusionProcessor.close()
                aggregationProcessor.close()
                latch.countDown()
                logger.info("流处理应用结束...")
            }
        });
    }
}
