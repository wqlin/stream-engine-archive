<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="INFO">
    <Properties>
        <Property name="log-dir">/appdata/log/stream</Property>
    </Properties>

    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <PatternLayout pattern="%d{yyyy-MM-dd HH:mm:ss.SSS} [%t] %-5level %logger{36} - %msg%n"/>
        </Console>

        <RollingFile name="interpolation-log" fileName="${log-dir}/interpolation/interpolation.log"
                     filePattern="${log-dir}/interpolation/interpolation-%d{yyyy-MM-dd}-%i.log">
            <PatternLayout pattern="%d{yyyy-MM-dd HH:mm:ss.SSS} [%t] [%-5level] - %msg%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy interval="1" modulate="true"/>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
        </RollingFile>

        <RollingFile name="interpolation-processor-log" fileName="${log-dir}/interpolation/interpolation-processor.log"
                     filePattern="${log-dir}/interpolation/interpolation-processor-%d{yyyy-MM-dd}-%i.log">
            <PatternLayout pattern="%d{yyyy-MM-dd HH:mm:ss.SSS} [%t] [%-5level] - %msg%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy interval="1" modulate="true"/>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
        </RollingFile>


        <RollingFile name="task-log" fileName="${log-dir}/interpolation/task.log"
                     filePattern="${log-dir}/interpolation/task-%d{yyyy-MM-dd}-%i.log">
            <PatternLayout pattern="%d{yyyy-MM-dd HH:mm:ss.SSS} [%t] [%-5level] - %msg%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy interval="1" modulate="true"/>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
        </RollingFile>

        <RollingFile name="fusion-log" fileName="${log-dir}/fusion/fusion.log"
                     filePattern="${log-dir}/fusion/fusion-%d{yyyy-MM-dd}-%i.log">
            <PatternLayout pattern="%d{yyyy-MM-dd HH:mm:ss.SSS} [%t] [%-5level] - %msg%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy interval="1" modulate="true"/>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
        </RollingFile>

        <RollingFile name="fusion-processor-log" fileName="${log-dir}/fusion/fusion-processor.log"
                     filePattern="${log-dir}/fusion/fusion-processor-%d{yyyy-MM-dd}-%i.log">
            <PatternLayout pattern="%d{yyyy-MM-dd HH:mm:ss.SSS} [%t] [%-5level] - %msg%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy interval="1" modulate="true"/>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
        </RollingFile>

        <RollingFile name="hour-aggregation-log" fileName="${log-dir}/aggregation/hour-aggregation.log"
                     filePattern="${log-dir}/aggregation/hour-aggregation-%d{yyyy-MM-dd}-%i.log">
            <PatternLayout pattern="%d{yyyy-MM-dd HH:mm:ss.SSS} [%t] [%-5level] - %msg%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy interval="1" modulate="true"/>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
        </RollingFile>

        <RollingFile name="day-aggregation-log" fileName="${log-dir}/aggregation/day-aggregation.log"
                     filePattern="${log-dir}/aggregation/day-aggregation-%d{yyyy-MM-dd}-%i.log">
            <PatternLayout
                    pattern="%d{yyyy-MM-dd HH:mm:ss.SSS} [%t] [%-5level] - %msg%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy interval="1" modulate="true"/>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
        </RollingFile>

        <RollingFile name="month-aggregation-log" fileName="${log-dir}/aggregation/month-aggregation.log"
                     filePattern="${log-dir}/aggregation/month-aggregation-%d{yyyy-MM-dd}-%i.log">
            <PatternLayout pattern="%d{yyyy-MM-dd HH:mm:ss.SSS} [%t] [%-5level] - %msg%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy interval="1" modulate="true"/>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
        </RollingFile>

        <RollingFile name="year-aggregation-log" fileName="${log-dir}/aggregation/year-aggregation.log"
                     filePattern="${log-dir}/aggregation/year-aggregation-%d{yyyy-MM-dd}-%i.log">
            <PatternLayout pattern="%d{yyyy-MM-dd HH:mm:ss.SSS} [%t] [%-5level] - %msg%n"/>
            <Policies>
                <TimeBasedTriggeringPolicy interval="1" modulate="true"/>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
        </RollingFile>

        <RollingFile name="runner-log" fileName="${log-dir}/runner.log"
                     filePattern="${log-dir}/runner-%d{yyyy-MM-dd}-%i.log">
            <PatternLayout pattern="%d{yyyy-MM-dd HH:mm:ss.SSS} [%t] [%-5level] - %msg%n"/>
            <Policies>
                <SizeBasedTriggeringPolicy size="20 MB"/>
            </Policies>
        </RollingFile>
    </Appenders>


    <Loggers>
        <!-- Every configuration must have a root logger. If one is not configured the default root LoggerConfig is ERROR with Console appender attached. -->
        <Root level="INFO">
            <AppenderRef ref="Console"/>
        </Root>

        <!-- additivity means, that parent-logger (in every case the root-logger) will also get the chance to log this stuff -->
        <Logger name="AverageValueInterpolationStrategyLogger" additivity="false" level="ALL">
            <AppenderRef ref="interpolation-log" level="ALL"/>
        </Logger>

        <Logger name="InterpolationProcessorLogger" additivity="false" level="ALL">
            <AppenderRef ref="interpolation-processor-log" level="ALL"/>
        </Logger>

        <Logger name="LogDataSuperviseTaskLogger" additivity="false" level="ALL">
            <AppenderRef ref="task-log" level="ALL"/>
        </Logger>

        <Logger name="HourAggregationLogger" additivity="false" level="ALL">
            <AppenderRef ref="hour-aggregation-log" level="ALL"/>
        </Logger>

        <Logger name="DayAggregationLogger" additivity="false" level="ALL">
            <AppenderRef ref="day-aggregation-log" level="ALL"/>
        </Logger>

        <Logger name="MonthAggregationLogger" additivity="false" level="ALL">
            <AppenderRef ref="month-aggregation-log" level="ALL"/>
        </Logger>

        <Logger name="YearAggregationLogger" additivity="false" level="ALL">
            <AppenderRef ref="year-aggregation-log" level="ALL"/>
        </Logger>

        <Logger name="FusionProcessorLogger" additivity="false" level="ALL">
            <AppenderRef ref="fusion-processor-log" level="ALL"/>
        </Logger>

        <Logger name="FusionLogger" additivity="false" level="ALL">
            <AppenderRef ref="fusion-log" level="ALL"/>
        </Logger>

        <Logger name="ProcessorRunnerLogger" additivity="false" level="ALL">
            <AppenderRef ref="runner-log" level="ALL"/>
        </Logger>

    </Loggers>
</Configuration>
