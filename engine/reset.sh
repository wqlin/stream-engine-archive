#!/usr/bin/env bash
kafka-streams-application-reset   --application-id fusion-processor --input-topics k_minute_mature
kafka-streams-application-reset   --application-id aggregation-processor --input-topics k_m_minute
kafka-streams-application-reset   --application-id interpolation-processor --input-topics k_minute_raw
