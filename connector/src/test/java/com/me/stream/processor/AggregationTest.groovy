package com.me.stream.processor;

import com.me.connector.config.RootConfig;
import com.me.connector.mysql.mapper.ManagementEntityMapper;
import com.me.entity.ManagementEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration

/**
 * Created by wqlin on 18-4-19 10:35.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = [RootConfig.class])
public class AggregationTest {
    @Autowired
    ManagementEntityMapper entityMapper;

    private double getAccValue(String tableName, String entityId, long startTime, long endTime) {
        List<ManagementEntity> result = entityMapper.getMEOrderByTimestamp(tableName, entityId, startTime, endTime)
        return result.inject(0.0, { a, b ->
            a + b.getValue()
        })
    }

    // 月的累积能耗数值应该等于年的累积能耗数值
    @Test
    public void testMonthAccValue() {
        String entityId = "0004";
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, Calendar.JANUARY, 0, 0, 0, 0);
        long startTime = calendar.getTimeInMillis();
        calendar.set(2016, Calendar.DECEMBER, 31, 23, 59, 59);
        long endTime = calendar.getTimeInMillis();
        double monthAccValue = getAccValue("t_m_month", entityId, startTime, endTime)
        println monthAccValue
        double yearAccValue = getAccValue("t_m_year", entityId, startTime, endTime)
        println yearAccValue
        // monthResult.forEach { it -> System.out.println(ME2Str(it)) }
    }
}
