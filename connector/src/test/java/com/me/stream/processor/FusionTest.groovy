package com.me.stream.processor;

import com.me.connector.config.RootConfig;
import com.me.connector.mysql.mapper.ManagementEntityMapper;
import com.me.entity.ManagementEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration

import java.util.concurrent.TimeUnit;


/**
 * Created by wqlin on 18-4-17 19:58.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = [RootConfig.class])
class FusionTest {
    @Autowired
    ManagementEntityMapper entityMapper;

    private static final String tableName = "t_k_mature";

    static final long FIFTEEN_MINUTE_IN_MILLIS = TimeUnit.MINUTES.toMillis(15);

    // 连续记录的时间戳相差应该为 15*60*1000 毫秒
    @Test
    public void testSuccessiveRecordTime() {
        String deviceId = "ABEA9265C1EF49F9A0FF5A274F1CDA37";
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, Calendar.JANUARY, 1, 1, 0, 0);
        long startTime = calendar.getTimeInMillis();
        calendar.set(2016, Calendar.DECEMBER, 31, 23, 59, 59);
        long endTime = calendar.getTimeInMillis();
        List<ManagementEntity> dataList = entityMapper.getMEOrderByTimestamp(tableName, deviceId, startTime, endTime);
        if (!dataList.isEmpty()) {
            long previousRecordTime = dataList.get(0).getTimestamp();
            for (int i = 1; i < dataList.size(); i++) {
                long currentRecordTime = dataList.get(i).getTimestamp();
                assert (currentRecordTime - previousRecordTime) == FIFTEEN_MINUTE_IN_MILLIS;
                previousRecordTime = currentRecordTime;
            }
        }
    }
}
