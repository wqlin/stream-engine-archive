package com.me.stream.processor;

import com.me.connector.config.RootConfig;
import com.me.connector.mysql.mapper.LogDataMapper;
import com.me.entity.LogData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by wqlin on 18-4-17 10:25.
 * 插值测试
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = [RootConfig.class])
public class InterpolationTest {
    @Autowired
    LogDataMapper logDataMapper;

    private static final String tableName = "t_k_mature";

    static final long FIFTEEN_MINUTE_IN_MILLIS = TimeUnit.MINUTES.toMillis(15);

    // 连续记录的时间戳相差应该为 15*60*1000 毫秒
    @Test
    public void testSuccessiveRecordTime() {
        String deviceId = "ABEA9265C1EF49F9A0FF5A274F1CDA37";
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, Calendar.JANUARY, 1, 1, 0, 0);
        long startTime = calendar.getTimeInMillis();
        calendar.set(2016, Calendar.DECEMBER, 31, 23, 59, 59);
        long endTime = calendar.getTimeInMillis();
        List<LogData> dataList = logDataMapper.getBetweenTimeRange(tableName, deviceId, startTime, endTime);
        if (!dataList.isEmpty()) {
            long previousRecordTime = dataList.get(0).getPeriodicAt();
            for (int i = 1; i < dataList.size(); i++) {
                long currentRecordTime = dataList.get(i).getPeriodicAt();
                assert (currentRecordTime - previousRecordTime) == FIFTEEN_MINUTE_IN_MILLIS;
                previousRecordTime = currentRecordTime;
            }
        }
    }

    // 当前记录的能耗值应该等于当前记录的累计读数减去上一个记录的累计读数
    @Test
    public void testSuccessiveRecordValue() {
        String deviceId = "ABEA9265C1EF49F9A0FF5A274F1CDA37";
        Calendar calendar = Calendar.getInstance();
        calendar.set(2016, Calendar.JANUARY, 1, 1, 0, 0);
        long startTime = calendar.getTimeInMillis();
        calendar.set(2016, Calendar.DECEMBER, 31, 23, 59, 59);
        long endTime = calendar.getTimeInMillis();
        List<LogData> dataList = logDataMapper.getBetweenTimeRange(tableName, deviceId, startTime, endTime);
        if (!dataList.isEmpty()) {
            double previousRecordAccValue = dataList.get(0).getAccValue();
            for (int i = 1; i < dataList.size(); i++) {
                double currentRecordAccValue = dataList.get(i).getAccValue();
                double currentRecordValue = dataList.get(i).getValue();
                assert Math.abs((currentRecordAccValue - previousRecordAccValue) - currentRecordValue) < 0.01;
                previousRecordAccValue = currentRecordAccValue;
            }
        }
    }
}
