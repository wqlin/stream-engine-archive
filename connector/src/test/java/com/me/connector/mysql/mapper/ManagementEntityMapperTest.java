package com.me.connector.mysql.mapper;

import com.me.connector.config.RootConfig;
import com.me.connector.mysql.entity.FourTuple;
import com.me.entity.ManagementEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

/**
 * Created by wqlin on 18-4-18 10:13.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {RootConfig.class})
public class ManagementEntityMapperTest {
    @Autowired
    ManagementEntityMapper entityMapper;

    private static final String tableName = "test";

    private static final String entityId = "0001";

    private static final long baseTimestamp = 1000;

    private static final double baseValue = 100.0;

    @Test
    public void testCreateHourTable() {
        entityMapper.createHourTableIfNotExist(tableName);
    }

    @Test
    public void testInsertExpect() {
        for (int i = 0; i < 10; i++) {
            double newExpect = baseValue + i * 10.0;
            long newTimestamp = baseTimestamp + i * 1000;
            int count = entityMapper.updateExpect(tableName, entityId, newTimestamp, newExpect);
            assert count == 1;
        }
    }

    @Test
    public void testGetFourTuples() {
        List<FourTuple> tuples = entityMapper.getFourTupleBetweenRange(tableName, entityId, baseTimestamp, baseTimestamp + 10 * 1000);
        tuples.forEach(System.out::println);
    }

    @Test
    public void testGetFourTuple() {
        for (int i = 0; i < 10; i++) {
            double newExpect = baseValue + i * 10.0;
            long newTimestamp = baseTimestamp + i * 1000;
            FourTuple tuple = entityMapper.getFourTuple(tableName, entityId, newTimestamp);
            System.out.println(tuple);
        }
    }

    @Test
    public void checkValueExists() throws Exception {
        ManagementEntity entity = ManagementEntity
                .newBuilder()
                .setEntityId("0001")
                .setValue(100.0)
                .setTimestamp(1524018984883L)
                .setLastUpdate(System.currentTimeMillis())
                .build();
        assert entityMapper.checkValueExists(tableName, entity);
    }

    @Test
    public void insert() throws Exception {
        for (int i = 0; i < 10; i++) {
            double value = baseValue + i * 10.0;
            long timestamp = baseTimestamp + i * 1000;
            ManagementEntity entity = ManagementEntity
                    .newBuilder()
                    .setEntityId("0001")
                    .setValue(value)
                    .setTimestamp(timestamp)
                    .setLastUpdate(timestamp)
                    .build();
            assert entityMapper.insert(tableName, entity) == 1;
        }
    }

    @Test
    public void updateValue() throws Exception {
        ManagementEntity entity = ManagementEntity
                .newBuilder()
                .setEntityId("0001")
                .setValue(100.0)
                .setTimestamp(1524018984883L)
                .setLastUpdate(System.currentTimeMillis())
                .build();
        assert entityMapper.updateValue(tableName, entity) == 1;
    }

}