package com.me.connector.detector;

import com.me.connector.config.RootConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import static com.me.connector.detector.Detectors.*;
import static com.me.utils.Utils.milliSecond2DateStr;

/**
 * Created by wqlin on 18-4-26 13:38.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {RootConfig.class})
public class DetectorsTest {
    private static final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    private boolean ensureTimestamp(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        if (calendar.get(Calendar.MINUTE) != 0)
            return false;
        else if (calendar.get(Calendar.SECOND) != 0)
            return false;
        else if (calendar.get(Calendar.MILLISECOND) == 0)
            return false;
        return true;
    }

    @Test
    public void testGenPreviousMonthTimestamp() throws Exception {
        String date = "2018-04-1";
        List<Long> result = genPreviousMonthTimestamp(formatter.parse(date), 5);
        for (Long timestamp : result) {
            if (ensureTimestamp(timestamp)) {
                throw new IllegalArgumentException(milliSecond2DateStr(timestamp));
            }
        }
        result.forEach(it ->
                System.out.println(milliSecond2DateStr(it))
        );
    }

    @Test
    public void testGenAfterMonthTimestamp() throws Exception {
        String date = "2018-04-1";
        List<Long> result = genAfterMonthTimestamp(formatter.parse(date), 5);
        for (Long timestamp : result) {
            if (ensureTimestamp(timestamp)) {
                throw new IllegalArgumentException(milliSecond2DateStr(timestamp));
            }
        }
        result.forEach(it ->
                System.out.println(milliSecond2DateStr(it))
        );
    }
}