package com.me.connector.controller;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.me.connector.config.RootConfig;
import com.me.utils.Utils;
import okhttp3.*;
import org.apache.kafka.common.protocol.types.Field;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * Created by wqlin on 18-5-2 17:16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {RootConfig.class})
public class StartControllerTest {
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final Type RESPONSE_TYPE = new TypeToken<Map<String, Object>>() {
    }.getType();
    private static final String BASE_URL = "http://127.0.0.1:9090/";
    private static final String ONLINE_ANOMALY_URL = BASE_URL + "anomaly/online";
    private static final String OFFLINE_ANOMALY_URL = BASE_URL + "anomaly/offline";

    private final OkHttpClient client = new OkHttpClient();

    private final Gson gson = new Gson();


    Map<String, Object> doPost(String requestContent, String requestURL) throws Exception {
        RequestBody requestBody = RequestBody.create(JSON, requestContent);

        Request request = new Request.Builder()
                .url(requestURL)
                .post(requestBody)
                .build();

        Map<String, Object> result = null;
        ResponseBody responseBody = client.newCall(request).execute().body();
        if (responseBody != null) {
            result = gson.fromJson(responseBody.string(), RESPONSE_TYPE);
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    private static List<Map<String, Object>> getData(Map<String, Object> response) {
        return (List<Map<String, Object>>) response.get("data");
    }

    @Test
    public void testOnlineNonExistId() throws Exception {
        String content = "{\"id\":\"001\"," +
                "\"startTime\":1514408400000," +
                "\"endTime\":1514728800000}";

        Map<String, Object> result = doPost(content, ONLINE_ANOMALY_URL);
        if (result != null) {
            List<Map<String, Object>> data = getData(result);
            System.out.println(data);
        }
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testOnlineLongerEndTime() throws Exception {
        String content = "{\"id\":\"0006\"," +
                "\"startTime\":1451577600000," +
                "\"endTime\":1514696400000}";
        Map<String, Object> result = doPost(content, ONLINE_ANOMALY_URL);
        if (result != null) {
            List<Map<String, Object>> data = getData(result);
            System.out.println(data.size());
            for (Map<String, Object> d : data) {
                System.out.println("expect: " + d.get("expect") + ", value: " + d.get("value") + ", timestamp: " + d.get("timestamp"));
            }
        }
    }

    @Test
    public void testOfflineNonExistId() throws Exception {
        String content = "{\"id\":\"001\"," +
                "\"date\":\"2015-04-01\"}";
        Map<String, Object> result = doPost(content, OFFLINE_ANOMALY_URL);
        if (result != null) {
            System.out.println(result);
        }
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testOfflineNormal() throws Exception {
        String content = "{\"id\":\"0006\"," +
                "\"date\":\"2016-04-01\"}";
        Map<String, Object> result = doPost(content, OFFLINE_ANOMALY_URL);
        if (result != null) {
            List<Map<String, Object>> data = getData(result);
            System.out.println(data.size());
            for (Map<String, Object> d : data) {
                System.out.println("expect: " + d.get("expect") + ", value: " + d.get("value") + ", timestamp: " + d.get("timestamp"));
            }
        }
    }
}