package com.me.connector.controller

import com.me.Constants
import com.me.connector.detector.Detectors
import com.me.connector.mysql.entity.FourTuple
import com.me.utils.TableManager
import com.me.connector.consumer.TopicConsumers
import com.me.connector.mysql.mapper.ManagementEntityMapper
import org.apache.ibatis.session.SqlSessionFactory
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController

import javax.annotation.PostConstruct
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.concurrent.atomic.AtomicLong
import java.util.stream.Collectors
import static com.me.utils.Utils.milliSecond2DateStr
import static com.me.connector.detector.DetectorUtils.ONE_HOUR_IN_MILLIS

/**
 * Created by wqlin on 18-4-18 10:49.
 */
@RestController
@CrossOrigin(maxAge = 3600L)
class StartController {
    private static final Logger logger = LogManager.getLogger("StartControllerLogger")
    private static final Logger requestLogger = LogManager.getLogger("RequestLogger")
    // private static final Logger offlineAnomalyLogger = LogManager.getLogger("OfflineAnomalyLogger")

    private static final String HOUR_TOPIC_CONSUMER_GROUP = "hourTopicConsumerGroup"
    private static final String DAY_TOPIC_CONSUMER_GROUP = "dayTopicConsumerGroup"
    private static final String MONTH_TOPIC_CONSUMER_GROUP = "monthTopicConsumerGroup"
    private static final String YEAR_TOPIC_CONSUMER_GROUP = "yearTopicConsumerGroup"

    @Autowired
    private SqlSessionFactory sessionFactory

    private static final AtomicLong count = new AtomicLong(0)

    private Thread hourConsumerThread, dayConsumerThread, monthConsumerThread, yearConsumerThread

    @Autowired
    private ManagementEntityMapper entityMapper

    @PostConstruct
    void startConsumer() {
        // 启动 consumer, 将逐时, 逐日, 逐月和逐年数据写入数据库
        if (count.incrementAndGet() == 1L) {
            logger.info("创建 consumers...")
            ManagementEntityMapper hourDataMapper = sessionFactory.openSession().getMapper(ManagementEntityMapper.class)
            TopicConsumers.HourTopicConsumer hourTopicConsumer = new TopicConsumers.HourTopicConsumer(hourDataMapper, HOUR_TOPIC_CONSUMER_GROUP, Constants.ME_HOUR_DATA_TOPIC)
            hourConsumerThread = new Thread(hourTopicConsumer, "HourConsumerThread")

            ManagementEntityMapper dayDataMapper = sessionFactory.openSession().getMapper(ManagementEntityMapper.class)
            TopicConsumers.DayTopicConsumer dayTopicConsumer = new TopicConsumers.DayTopicConsumer(dayDataMapper, DAY_TOPIC_CONSUMER_GROUP, Constants.ME_DAY_DATA_TOPIC)
            dayConsumerThread = new Thread(dayTopicConsumer, "DayConsumerThread")

            ManagementEntityMapper monthDataMapper = sessionFactory.openSession().getMapper(ManagementEntityMapper.class)
            TopicConsumers.MonthTopicConsumer monthTopicConsumer = new TopicConsumers.MonthTopicConsumer(monthDataMapper, MONTH_TOPIC_CONSUMER_GROUP, Constants.ME_MONTH_DATA_TOPIC)
            monthConsumerThread = new Thread(monthTopicConsumer, "MonthConsumerThread")

            ManagementEntityMapper yearDataMapper = sessionFactory.openSession().getMapper(ManagementEntityMapper.class)
            TopicConsumers.YearTopicConsumer yearTopicConsumer = new TopicConsumers.YearTopicConsumer(yearDataMapper, YEAR_TOPIC_CONSUMER_GROUP, Constants.ME_YEAR_DATA_TOPIC)
            yearConsumerThread = new Thread(yearTopicConsumer, "YearConsumerThread")

            hourConsumerThread.start()
            dayConsumerThread.start()
            monthConsumerThread.start()
            yearConsumerThread.start()
            logger.info("创建 consumers 完成")
        }
    }

    @GetMapping("/")
    @ResponseBody
    public String hello() {
        return "Hello World"
    }

    private static final int MAXIMUM_SIZE = 30 * 24 -1
    /**
     * 在线预测
     */
    @PostMapping("/anomaly/online")
    public ResponseEntity<Map<String, Object>> getOnlineAnomaly(@RequestBody Map<String, Object> request) {
        requestLogger.info("[Online Anomaly Request]: " + request)
        ResultMap resultMap = new ResultMap()
        String entityId = ""
        Long startTime = 0L
        Long endTime = 0L
        if (!request.containsKey("id"))
            resultMap.onFailure(404, "NotFoundError", "id not exist in parameter")
        else if (!request.containsKey("startTime"))
            resultMap.onFailure(404, "NotFoundError", "startTime not exist in parameter")
        else if (!request.containsKey("endTime"))
            resultMap.onFailure(404, "NotFoundError", "endTime not exist in parameter")
        else {
            try {
                entityId = request.get("id")
                String tableName = TableManager.getTable(entityId, TableManager.TableType.Hour)
                startTime = (Long) request.get("startTime")
                endTime = (Long) request.get("endTime")
                if (startTime > endTime) {
                    resultMap.onFailure(500, "Error", "startTime: " + startTime + " (" + milliSecond2DateStr(startTime) + ") is greater than endTime: " + endTime + " (" + milliSecond2DateStr(endTime) + ")")
                } else {
                    if (resultMap.containsKey("test"))
                        tableName = "test"
                    if (endTime - startTime > ONE_HOUR_IN_MILLIS * MAXIMUM_SIZE)
                        endTime = startTime + ONE_HOUR_IN_MILLIS * MAXIMUM_SIZE

                    List<FourTuple> tuples = entityMapper.getFourTupleBetweenRange(tableName, entityId, startTime, endTime)

                    List<Map<String, Object>> data = tuples.stream().filter { it != null }.map { tuple ->
                        Map<String, Object> m = new HashMap<>()
                        m.put("expect", tuple.getExpect())
                        m.put("value", tuple.getValue())
                        m.put("timestamp", tuple.getTimestamp())
                        m
                    }.collect(Collectors.toList())
                    resultMap.onSuccess(data)
                }
            } catch (Exception e) {
                logger.error(e)
                resultMap.onFailure(500, "Error", "Error in processing online anomaly request, id: " + entityId + ", start time: " + startTime + ", end time: " + endTime)
            }
        }
        return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK)
    }

    private static final DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd")
    /**
     * 离线检测
     */
    @PostMapping("/anomaly/offline")
    public ResponseEntity<Map<String, Object>> getOfflineAnomaly(@RequestBody Map<String, Object> request) {
        requestLogger.info("[Offline Anomaly Request]: " + request)
        ResultMap resultMap = new ResultMap()
        if (!request.containsKey("id"))
            resultMap.onFailure(404, "NotFoundError", "id not exist in parameter")
        else if (!request.containsKey("date"))
            resultMap.onFailure(404, "NotFoundError", "date not exist in parameter")
        String entityId = request.get("id")
        String dateStr = request.get("date")
        Date date
        try {
            date = formatter.parse(dateStr)
            List<FourTuple> tuples = Detectors.offlineDetect(entityId, date, entityMapper)

            List<Map<String, Object>> data = tuples.stream().map { tuple ->
                Map<String, Object> m = new HashMap<>()
                m.put("expect", tuple.getExpect())
                m.put("value", tuple.getValue())
                m.put("timestamp", tuple.getTimestamp())
                m
            }.collect(Collectors.toList())
            resultMap.onSuccess(data)
        } catch (ParseException e) {
            logger.error(e)
            resultMap.onFailure(500, "Error", "Error in parsing date: " + dateStr)
        } catch (Exception e) {
            logger.error(e)
            resultMap.onFailure(500, "Error", "Error in processing offline anomaly request, id: " + entityId + ", date: " + dateStr)
        }
        return new ResponseEntity<Map<String, Object>>(resultMap, HttpStatus.OK)
    }
}
