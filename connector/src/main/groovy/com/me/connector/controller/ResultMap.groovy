package com.me.connector.controller

/**
 * Created by wqlin on 18-4-21 23:35.
 */
public class ResultMap extends HashMap<String, Object> {
    public ResultMap() {
        super()
    }

    public void onSuccess(List<Map<String, Object>> data) {
        this.put("data", data)
    }

    public void onFailure(int code, String name, String message) {
        this.put("code", code)
        this.put("name", name)
        this.put("message", message)
    }
}
