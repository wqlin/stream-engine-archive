package com.me.connector.consumer

import com.me.connector.detector.Detectors
import com.me.connector.mysql.mapper.ManagementEntityMapper
import com.me.entity.ManagementEntity
import com.me.utils.TableManager
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.consumer.ConsumerRecords
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.apache.logging.log4j.Level

import static com.me.utils.Utils.ME2Str
import static com.me.utils.Utils.milliSecond2DateStr

/**
 * Created by wqlin on 18-4-18 11:19.
 */
class TopicConsumers {
    static class HourTopicConsumer extends BaseConsumer {
        private static final Logger logger = LogManager.getLogger("HourConsumerLogger")
        private static final Logger onlineAnomalyLogger = LogManager.getLogger("OnlineAnomalyLogger")

        HourTopicConsumer(ManagementEntityMapper entityMapper, String groupId, String... topics) {
            super(entityMapper, groupId, topics)
        }

        void createTableIfNotExist() {
            List<String> hourTables = TableManager.getHourTables()
            for (String table : hourTables)
                entityMapper.createHourTableIfNotExist(table)
        }

        String getTableName(String key) {
            return TableManager.getTable(key, TableManager.TableType.Hour)
        }

        void log(Level level, Object message) {
            logger.log(level, message)
        }

        private void setExpectFor(ManagementEntity entity, double expect) {
            String tableName = getTableName(entity.getEntityId())
            int result = entityMapper.updateExpect(tableName, entity.getEntityId(), entity.getTimestamp(), expect)
            if (result == 1)
                onlineAnomalyLogger.info("[更新] 设置ID " + entity.getEntityId() + " 在 " +
                        milliSecond2DateStr(entity.getTimestamp()) + " 的期望值为: " +
                        String.format("%.4f", expect) + " 成功")
            else
                onlineAnomalyLogger.error("[更新] 设置ID " + entity.getEntityId() + " 在 " +
                        milliSecond2DateStr(entity.getTimestamp()) + " 的期望值为: " +
                        String.format("%.4f", expect) + " 失败")
        }

        private static final Calendar calendar = Calendar.getInstance()

        // 是否是整点时间
        private static boolean isSharpTime(long timestamp) {
            calendar.setTimeInMillis(timestamp)
            return calendar.get(Calendar.MINUTE) == 0
        }

        @Override
        public void run() {
            while (true) {
                ConsumerRecords<String, ManagementEntity> records = kafkaConsumer.poll(POLL_THRESHOLD)
                if (records.isEmpty())
                    log(Level.ERROR, "Kafka sink consumer 没有收到新记录")
                for (ConsumerRecord<String, ManagementEntity> record : records) {
                    ManagementEntity entity = record.value()
                    insertOrUpdate(entity)
                    if (isSharpTime(entity.getTimestamp())) {
                        try {
                            double expect = Detectors.onlinePredict(record.value(), entityMapper)
                            setExpectFor(entity, expect)
                        } catch (Exception e) {
                            println e
                            onlineAnomalyLogger.error(e)
                        }
                    }
                }
            }
        }
    }

    static class DayTopicConsumer extends BaseConsumer {
        private static final Logger logger = LogManager.getLogger("DayConsumerLogger")

        DayTopicConsumer(ManagementEntityMapper entityMapper, String groupId, String... topics) {
            super(entityMapper, groupId, topics)
        }

        void createTableIfNotExist() {
            List<String> dayTables = TableManager.getDayTables()
            for (String table : dayTables)
                entityMapper.createTableIfNotExist(table)
        }

        String getTableName(String key) {
            return TableManager.getTable(key, TableManager.TableType.Day)
        }

        void log(Level level, Object message) {
            logger.log(level, message)
        }
    }

    static class MonthTopicConsumer extends BaseConsumer {
        private static final Logger logger = LogManager.getLogger("MonthConsumerLogger")

        MonthTopicConsumer(ManagementEntityMapper entityMapper, String groupId, String... topics) {
            super(entityMapper, groupId, topics)
        }

        void createTableIfNotExist() {
            List<String> monthTables = TableManager.getMonthTables()
            for (String table : monthTables)
                entityMapper.createTableIfNotExist(table)
        }

        String getTableName(String key) {
            return TableManager.getTable(key, TableManager.TableType.Month)
        }

        void log(Level level, Object message) {
            logger.log(level, message)
        }
    }

    static class YearTopicConsumer extends BaseConsumer {
        private static final Logger logger = LogManager.getLogger("YearConsumerLogger")

        YearTopicConsumer(ManagementEntityMapper entityMapper, String groupId, String... topics) {
            super(entityMapper, groupId, topics)
        }

        void createTableIfNotExist() {
            List<String> yearTables = TableManager.getYearTables()
            for (String table : yearTables)
                entityMapper.createTableIfNotExist(table)
        }

        String getTableName(String key) {
            return TableManager.getTable(key, TableManager.TableType.Year)
        }

        void log(Level level, Object message) {
            logger.log(level, message)
        }
    }
}
