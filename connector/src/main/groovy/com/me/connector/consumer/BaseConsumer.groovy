package com.me.connector.consumer

import com.me.Constants
import com.me.connector.mysql.mapper.ManagementEntityMapper
import com.me.entity.ManagementEntity
import io.confluent.kafka.serializers.KafkaAvroDeserializer
import org.apache.logging.log4j.Level
import java.util.concurrent.TimeUnit

import static com.me.utils.Utils.ME2Str
import io.confluent.kafka.streams.serdes.avro.SpecificAvroDeserializer
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.clients.consumer.ConsumerRecords
import org.apache.kafka.clients.consumer.KafkaConsumer

/**
 * Created by wqlin on 18-4-17 20:58.
 * 将逐时, 逐天, 逐月和逐年的数据写入 MySQL
 * 基类
 */
public abstract class BaseConsumer implements Runnable {
    // poll 最大超时时间为 30 分钟
    protected static final int POLL_THRESHOLD = TimeUnit.MINUTES.toMillis(30)

    protected KafkaConsumer<String, ManagementEntity> kafkaConsumer

    protected ManagementEntityMapper entityMapper

    public volatile boolean stopThread = false

    BaseConsumer(ManagementEntityMapper entityMapper, String groupId, String... topics) {
        this.entityMapper = entityMapper
        Properties props = new Properties()
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, Constants.BOOTSTRAP_SERVERS_CONFIG)
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId)
        props.put("schema.registry.url", Constants.SCHEMA_REGISTRY_URL)
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "100")
        props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true")
        props.put("specific.avro.reader", "true")
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class)
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, SpecificAvroDeserializer.class)
        kafkaConsumer = new KafkaConsumer<>(props)
        List<String> subscribeTopics = new ArrayList<>()
        for (int i = 0; i < topics.length; i++)
            subscribeTopics.add(topics[i])
        kafkaConsumer.subscribe(subscribeTopics)
        createTableIfNotExist()
        log(Level.INFO, "[新建] 创建 consumer, group: " + groupId + ", 订阅主题: " + subscribeTopics)
    }

    abstract void createTableIfNotExist()

    abstract String getTableName(String key)

    abstract void log(Level level, Object message)

    void stop() {
        stopThread = true
    }

    protected void insertOrUpdate(ManagementEntity value) {
        String tableName = getTableName(value.getEntityId())
        if (entityMapper.checkValueExists(tableName, value)) {
            log(Level.INFO, "[更新]: " + tableName + " 记录: " + ME2Str(value))
            int result = entityMapper.updateValue(tableName, value)
            if (result == 0)
                log(Level.ERROR, "[更新失败]: " + tableName + " 记录: " + ME2Str(value))
        } else {
            log(Level.INFO, "[插入]: " + tableName + " 记录: " + ME2Str(value))
            int result = entityMapper.insert(tableName, value)
            if (result == 0)
                log(Level.ERROR, "[插入失败]: " + tableName + " 记录: " + ME2Str(value))
        }
    }

    @Override
    public void run() {
        while (!stopThread) {
            try {
                ConsumerRecords<String, ManagementEntity> records = kafkaConsumer.poll(POLL_THRESHOLD)
                if (records.isEmpty())
                    log(Level.ERROR, "Kafka sink consumer 没有收到新记录")
                for (ConsumerRecord<String, ManagementEntity> record : records) {
                    insertOrUpdate(record.value())
                }
            } catch (Exception e) {
                log(Level.ERROR, e)
            }
        }
    }
}
