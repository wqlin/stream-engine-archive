package com.me.connector.mysql.entity

import static com.me.utils.Utils.milliSecond2DateStr

/**
 * Created by wqlin on 18-4-24 10:22.
 */
public class FourTuple {
    private String entity_id

    private double expect

    private double value

    private long timestamp

    public FourTuple() {
        this.entity_id = ""
        this.expect = this.value = 0.0
        this.timestamp = 0L
    }

    public FourTuple(String entityId, double expect, double value, long timestamp) {
        this.entity_id = entityId
        this.expect = expect
        this.value = value
        this.timestamp = timestamp
    }

    String getEntity_id() {
        return entity_id
    }

    void setEntity_id(String entity_id) {
        this.entity_id = entity_id
    }

    double getExpect() {
        return expect
    }

    void setExpect(double expect) {
        this.expect = expect
    }

    double getValue() {
        return value
    }

    void setValue(double value) {
        this.value = value
    }

    long getTimestamp() {
        return timestamp
    }

    void setTimestamp(long timestamp) {
        this.timestamp = timestamp
    }

    public String toString() {
        return "实体 ID: " + entity_id +
                "\t 预测值: " + String.format("%.4f", expect) +
                "\t 采集值: " + String.format("%.4f", value) +
                "\t 时间戳: " + milliSecond2DateStr(timestamp)

    }
}
