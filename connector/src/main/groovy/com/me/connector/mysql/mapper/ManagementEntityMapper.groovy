package com.me.connector.mysql.mapper

import com.me.connector.mysql.entity.FourTuple
import com.me.entity.ManagementEntity
import org.apache.ibatis.annotations.Param
import org.springframework.stereotype.Component

/**
 * Created by wqlin on 18-4-17 10:21.
 */
@Component
public interface ManagementEntityMapper {
    /**
     * 逐天, 逐月和逐年数据表
     * @param tableName
     * @return
     */
    int createTableIfNotExist(@Param("tableName") String tableName)

    /**
     * 逐时数据表, 包含异常检测预测值
     * @param tableName
     * @return
     */
    int createHourTableIfNotExist(@Param("tableName") String tableName)


    List<FourTuple> getFourTupleBetweenRange(@Param("tableName") String tableName,
                                             @Param("entityId") String entityId,
                                             @Param("startTime") long startTime,
                                             @Param("endTime") long endTime)

    FourTuple getFourTuple(@Param("tableName") String tableName,
                           @Param("entityId") String entityId,
                           @Param("timestamp") long timestamp)

    boolean checkValueExists(@Param("tableName") String tableName,
                             @Param("entity") ManagementEntity entity)

    int insert(@Param("tableName") String tableName,
               @Param("entity") ManagementEntity entity)

    int updateValue(@Param("tableName") String tableName,
                    @Param("entity") ManagementEntity entity)

    int updateExpect(@Param("tableName") String tableName,
                     @Param("entityId") String entityId,
                     @Param("timestamp") long timestamp,
                     @Param("expect") double expect)
}
