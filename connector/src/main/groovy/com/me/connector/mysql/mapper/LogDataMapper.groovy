package com.me.connector.mysql.mapper

import com.me.entity.LogData
import org.apache.ibatis.annotations.Param
import org.springframework.stereotype.Component

/**
 * Created by wqlin on 18-4-17 10:04.
 */
@Component
public interface LogDataMapper {
    List<LogData> getBetweenTimeRange(@Param("tableName") String tableName,
                                      @Param("deviceId") String deviceId,
                                      @Param("startTime") long startTime,
                                      @Param("endTime") long endTime)
}
