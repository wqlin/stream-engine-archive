package com.me.connector.detector

import com.yahoo.egads.control.AnomalyDetector
import com.yahoo.egads.control.ModelAdapter
import com.yahoo.egads.control.ProcessableObjectFactory
import com.yahoo.egads.data.TimeSeries

/**
 * Created by wqlin on 18-4-25 11:06.
 */
public class OnlineAnomalyDetector implements IAnomalyDetector {
    private ModelAdapter ma
    private AnomalyDetector ad


    OnlineAnomalyDetector(ModelAdapter ma, AnomalyDetector ad) {
        this.ma = ma
        this.ad = ad
    }

    public void updateModelAdapter(ModelAdapter ma) {
        this.ma = ma
    }

    public void updateAnomalyDetector(AnomalyDetector ad) {
        this.ad = ad
    }

    /**
     * 返回最后一个结果, 不使用 AnomalyDetector 进行检测
     * @throws Exception
     */
    public TimeSeries.DataSequence predict() throws Exception {
        ma.reset() // 重置模型
        ma.train()

        // 因为 metric 在 ModelAdapter 的可见性是 protected, 所以使用 groovy 的 Direct field access operator
        ArrayList<TimeSeries.DataSequence> list = ma.forecast(
                ma.@metric.startTime(), ma.@metric.lastTime())

        if (list.isEmpty())
            return null
        else
            return list.get(list.size() - 1)
    }

    public static void main(String[] args) {
        Random random = new Random()
        long startTime = 1524652084L
        double value = 100.0
        TimeSeries ts = new TimeSeries()
        for (int i = 0; i < 14; i++) {
            ts.append(startTime + i * 1000, (value + random.nextInt(10) * 100.0).toFloat())
        }
        println "TS object: "
        println ts
        // only need one TimeSeries object
        Properties config = new Properties()
        config.put("DETECTION_WINDOW_START_TIME", "0")
        config.put("MAX_ANOMALY_TIME_AGO", "999999999")
        config.put("AGGREGATION", "1")

        config.put("OP_TYPE", "DETECT_ANOMALY")

        config.put("TS_MODEL", "OlympicModel")

        config.put("AD_MODEL", "ExtremeLowDensityModel")

        // config for Olympic Forecast Model
        config.put("TIME_SHIFTS", "0,1")
        config.put("BASE_WINDOWS", "1,1")
        config.put("PERIOD", "0")
        config.put("NUM_WEEKS", "2")
        config.put("NUM_TO_DROP", "0")
        config.put("DYNAMIC_PARAMETERS", "1")

        // config for NaiveModel
        // properties.put("WINDOW_SIZE", "0.1")

        // config for ExtremeLowDensityModel & DBScanModel
        config.put("AUTO_SENSITIVITY_ANOMALY_PCNT", 0.01)
        config.put("AUTO_SENSITIVITY_SD", 3.0)

        ModelAdapter ma = ProcessableObjectFactory.buildTSModel(ts, config)
        AnomalyDetector ad = ProcessableObjectFactory.buildAnomalyModel(ts, config)
        OnlineAnomalyDetector onlineAnomalyDetector = new OnlineAnomalyDetector(ma, ad)
        TimeSeries.DataSequence result = onlineAnomalyDetector.predict()
        println result.values
        println result.times

        // 那么根据每一个收到的 entity 实体, 首先读取数据库, 构建数据集, 然后预测这一个时刻的值


//        ProcessableObject object = ProcessableObjectFactory.create(ts, config)
//        object.process()
    }
}
