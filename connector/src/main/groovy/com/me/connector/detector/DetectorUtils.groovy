package com.me.connector.detector

import com.me.connector.mysql.entity.FourTuple

import java.util.concurrent.TimeUnit
import java.util.stream.Collectors

import static com.me.utils.Utils.milliSecond2DateStr
import static com.me.utils.Utils.round

/**
 * Created by wqlin on 18-4-25 14:17.
 */
public class DetectorUtils {
    /**
     * 在线预测时使用的窗口大小
     */

    public static final long ONE_HOUR_IN_MILLIS = TimeUnit.HOURS.toMillis(1)

    public static final long ONE_DAY_IN_MILLIS = TimeUnit.DAYS.toMillis(1)

    public static List<Long> genTimestampBySubtract(long base, long step, int total = 1) {
        List<Long> result = new ArrayList<>()
        for (int i = 1; i <= total; i++)
            result.add(base - i * step)
        return result
    }

    public static List<Long> genTimestampByPlus(long base, long step, int total = 1) {
        List<Long> result = new ArrayList<>()
        for (int i = 1; i <= total; i++)
            result.add(base + i * step)
        return result
    }

    public static FourTuple mergeTupleValues(List<FourTuple> tuples, long timestamp) {
        if (tuples.isEmpty())
            throw new IllegalArgumentException("tuples is empty")
        List<String> ids = tuples.stream().map { it ->
            it?.getEntity_id()
        }.distinct().collect(Collectors.toList())
        if (ids.size() != 1)
            throw new IllegalArgumentException("entity id in tuples must be unique")
        double average = round(tuples.stream().mapToDouble { it.getValue() }.sum() / tuples.size(), 4)
        FourTuple result = new FourTuple(ids.first(), 0.0, average, timestamp)
        return result
    }

    public static void main(String[] args) {
        long currentTimeInMillis = System.currentTimeMillis()
        Calendar calendar = Calendar.getInstance()
        calendar.setTimeInMillis(currentTimeInMillis)
        calendar.clear(Calendar.SECOND)
        calendar.clear(Calendar.MILLISECOND)

        long time = calendar.getTimeInMillis()
        List<Long> before = genTimestampBySubtract(time, 3)
        List<Long> after = genTimestampByPlus(time, 3)
        println "Current: " + milliSecond2DateStr(time)
        before.forEach {
            println "Before: " + milliSecond2DateStr(it)
        }
        after.forEach {
            println "After: " + milliSecond2DateStr(it)
        }
        println ONE_HOUR_IN_MILLIS
        println ONE_DAY_IN_MILLIS
    }
}
