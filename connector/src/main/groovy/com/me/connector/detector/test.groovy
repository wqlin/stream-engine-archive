package com.me.connector.detector

import com.yahoo.egads.control.ModelAdapter
import com.yahoo.egads.control.ProcessableObject
import com.yahoo.egads.control.ProcessableObjectFactory
import com.yahoo.egads.data.Anomaly
import com.yahoo.egads.data.TimeSeries

/**
 * Created by wqlin on 18-4-24 15:02.
 * egads 测试类
 */
class test {
    public static void main(String[] args) {
        // 1. create a list of time series object, what is TimeSeries
        // 2. For each TimeSeries object, create an ProcessableObjectFactory
        // 3. DataSequence
        // name and metric
        // 1. DetectAnomalyProcessable 2. UpdateModelProcessable
        // 3. TransformInputProcessable

        // 精度是秒
        long startTime = System.currentTimeMillis() / 1000
        double value = 100.0
        TimeSeries ts = new TimeSeries()
        for (int i = 0; i < 14; i++) {
            ts.append(startTime + i * 1000, (100.0 + i * 100.0).toFloat())
        }
        long start = ts.startTime() + 7 * 1000
        long end = ts.startTime() + 12 * 1000
        println "TS object: "
        println ts
        println ""
        // only need one TimeSeries object
        Properties config = new Properties()
        config.put("DETECTION_WINDOW_START_TIME", "0")
        config.put("MAX_ANOMALY_TIME_AGO", "999999999")
        config.put("AGGREGATION", "1")

        config.put("OP_TYPE", "DETECT_ANOMALY")

        // properties.put("OP_TYPE", "TRANSFORM_INPUT")

        config.put("TS_MODEL", "OlympicModel")

        config.put("AD_MODEL", "ExtremeLowDensityModel")

        // properties.put("OUTPUT", "ANOMALY_DB")

        // config for Olympic Forecast Model
        config.put("TIME_SHIFTS", "0,1")
        config.put("BASE_WINDOWS", "24,168")
        config.put("PERIOD", "0")
        config.put("NUM_WEEKS", "2")
        config.put("NUM_TO_DROP", "0")
        config.put("DYNAMIC_PARAMETERS", "1")

        // config for NaiveModel
        // properties.put("WINDOW_SIZE", "0.1")

        // config for ExtremeLowDensityModel & DBScanModel
        config.put("AUTO_SENSITIVITY_ANOMALY_PCNT", 0.01)
        config.put("AUTO_SENSITIVITY_SD", 3.0)

        ModelAdapter ma = ProcessableObjectFactory.buildTSModel(ts, config)

        println ma==null
        ma.reset()
        ma.train()
        List<TimeSeries.DataSequence> values = ma.forecast(start, end)
//        for (TimeSeries.DataSequence sequence : values) {
//            println "value: " + sequence
//        }
        // IAnomalyDetector ad = ProcessableObjectFactory.buildAnomalyModel(ts, config)
    }
}
