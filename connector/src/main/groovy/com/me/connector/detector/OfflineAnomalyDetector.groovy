package com.me.connector.detector

import com.me.utils.Utils
import com.yahoo.egads.control.AnomalyDetector
import com.yahoo.egads.control.ModelAdapter
import com.yahoo.egads.control.ProcessableObjectFactory
import com.yahoo.egads.data.Anomaly
import com.yahoo.egads.data.TimeSeries

/**
 * Created by wqlin on 18-4-25 11:28.
 */
public class OfflineAnomalyDetector implements IAnomalyDetector {
    private ModelAdapter ma
    private AnomalyDetector ad

    OfflineAnomalyDetector(ModelAdapter ma, AnomalyDetector ad) {
        this.ma = ma
        this.ad = ad
    }


    public void updateModelAdapter(ModelAdapter ma) {
        this.ma = ma
    }

    public void updateAnomalyDetector(AnomalyDetector ad) {
        this.ad = ad
    }

    public TimeSeries.DataSequence detect() throws Exception {
        ma.reset()
        ma.train()

        ArrayList<TimeSeries.DataSequence> list = ma.forecast(
                ma.@metric.startTime(), ma.@metric.lastTime())

        if (list.isEmpty())
            return null
        else
            return list.last()
    }

    public static void main(String[] args) {
        Random random = new Random()
        long startTime = 1524652084L
        double value = 100.0
        TimeSeries ts = new TimeSeries()
        for (int i = 1; i <= 100; i++) {
            if (i % 10 == 0)
                ts.append(startTime + i * 100000, 9999999999.0.toFloat())
            else
                ts.append(startTime + i * 100000, (value + random.nextInt(10) * 100.0).toFloat())
        }
        Float[] values = ts.data.values
        Long[] times = ts.data.times
        for (int i = 0; i < times.length; i++) {
            println Utils.milliSecond2DateStr(times[i]*1000) + " " + values[i]
        }

        // only need one TimeSeries object
        Properties config = new Properties()
        config.put("DETECTION_WINDOW_START_TIME", "0")
        config.put("MAX_ANOMALY_TIME_AGO", "999999999")
        config.put("AGGREGATION", "1")

        config.put("OP_TYPE", "DETECT_ANOMALY")

        // properties.put("OP_TYPE", "TRANSFORM_INPUT")

        config.put("TS_MODEL", "OlympicModel")

        config.put("AD_MODEL", "ExtremeLowDensityModel")

        // properties.put("OUTPUT", "ANOMALY_DB")

        // config for Olympic Forecast Model
        config.put("TIME_SHIFTS", "0,1")
        config.put("BASE_WINDOWS", "24,168")
        config.put("PERIOD", "0")
        config.put("NUM_WEEKS", "2")
        config.put("NUM_TO_DROP", "0")
        config.put("DYNAMIC_PARAMETERS", "1")

        // config for NaiveModel
        // properties.put("WINDOW_SIZE", "0.1")

        // config for ExtremeLowDensityModel & DBScanModel
        config.put("AUTO_SENSITIVITY_ANOMALY_PCNT", 0.01)
        config.put("AUTO_SENSITIVITY_SD", 3.0)

        ModelAdapter ma = ProcessableObjectFactory.buildTSModel(ts, config)
        AnomalyDetector ad = ProcessableObjectFactory.buildAnomalyModel(ts, config)

        OfflineAnomalyDetector offlineAnomalyDetector = new OfflineAnomalyDetector(ma, ad)
    }
}
