package com.me.connector.detector

import com.me.connector.mysql.entity.FourTuple
import com.me.connector.mysql.mapper.ManagementEntityMapper
import com.me.entity.ManagementEntity
import com.me.utils.TableManager
import com.yahoo.egads.control.AnomalyDetector
import com.yahoo.egads.control.ModelAdapter
import com.yahoo.egads.control.ProcessableObjectFactory
import com.yahoo.egads.data.TimeSeries
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.util.stream.Collectors

import static com.me.connector.detector.DetectorUtils.*
import static com.me.utils.Utils.ME2Str
import static com.me.utils.Utils.milliSecond2DateStr
import static com.me.utils.Utils.round

/**
 * Created by wqlin on 18-4-25 14:41.
 */
public class Detectors {
    // 使用过去 30 天的逐时数据做预测
    private static final int ONLINE_TRAIN_DAYS = 30
    private static final int ONLINE_WINDOW_SIZE = 2

    private static final Logger onlineAnomalyLogger = LogManager.getLogger("OnlineAnomalyLogger")
    private static final Logger offlineAnomalyLogger = LogManager.getLogger("OfflineAnomalyLogger")

    // 毫秒转换成秒
    private static long millis2sec(long millisecond) {
        return millisecond / 1000L
    }

    /**
     * 在线预测 entityId 在 timestamp 的能耗值
     * @param entity 收到的 ManagementEntity 记录
     * @return entity 在 timestamp
     */
    public static double onlinePredict(
            final ManagementEntity entity, ManagementEntityMapper entityMapper) throws Exception {
        long timestamp = entity.getTimestamp()

        // 获取与 timestamp 有同一个小时的历史 timestamp
        List<Long> sameHourInPreviousDay = genTimestampBySubtract(timestamp, ONE_DAY_IN_MILLIS, ONLINE_TRAIN_DAYS)

        // 对每一个 timestamp 做一个窗口滑动
        List<List<Long>> windowHourInPreviousDay = sameHourInPreviousDay.stream().map { t ->
            List<Long> tmp = genTimestampBySubtract(t, ONE_HOUR_IN_MILLIS, ONLINE_WINDOW_SIZE)
            tmp.add(t)
            tmp
        }.collect(Collectors.toList())

        String tableName = TableManager.getTable(entity.getEntityId(), TableManager.TableType.Hour)

        List<FourTuple> tuples = windowHourInPreviousDay.stream().map { it ->
            it.stream().map { t ->
                entityMapper.getFourTuple(tableName, entity.getEntityId(), t)
            }.filter {
                it != null
            }.collect(Collectors.toList())
            // it.last 存储的是与 timestamp 具有同一个小时的时间戳
        }.filter {
            (!it != null) && (!it.isEmpty())
        }.map { it -> mergeTupleValues(it, it.last().getTimestamp()) }.collect(Collectors.toList())

        if (tuples.isEmpty())
            return entity.getValue()

        tuples.sort(new Comparator<FourTuple>() {
            @Override
            int compare(FourTuple o1, FourTuple o2) {
                return Long.compare(o1.getTimestamp(), o2.getTimestamp())
            }
        })

        onlineAnomalyLogger.info("[预测]: ID: " + entity.entityId + ", 时间戳: " + milliSecond2DateStr(entity.getTimestamp()) +
                ", 能耗数据: " + String.format("%.4f", entity.getValue())
                + ", 训练数据起始时间戳: " + milliSecond2DateStr(tuples.first().getTimestamp()) + ", 训练数据终止时间戳:" + milliSecond2DateStr(tuples.last().getTimestamp()))
        // 基于 tuples 构建 TimeSeries
        TimeSeries ts = new TimeSeries()
        tuples.forEach { it ->
            long time = millis2sec(it.getTimestamp())
            float value = it.getValue().toFloat()
            ts.append(time, value)
        }

        Properties config = new Properties()
        config.put("DETECTION_WINDOW_START_TIME", "0")
        config.put("MAX_ANOMALY_TIME_AGO", "999999999")
        config.put("AGGREGATION", "1")

        config.put("OP_TYPE", "DETECT_ANOMALY")

        config.put("TS_MODEL", "OlympicModel")

        config.put("AD_MODEL", "ExtremeLowDensityModel")

        // config for Olympic Forecast Model
        config.put("TIME_SHIFTS", "0,1")
        config.put("BASE_WINDOWS", "1,1")
        config.put("PERIOD", "0")
        config.put("NUM_WEEKS", "2")
        config.put("NUM_TO_DROP", "0")
        config.put("DYNAMIC_PARAMETERS", "1")

        // config for ExtremeLowDensityModel & DBScanModel
        config.put("AUTO_SENSITIVITY_ANOMALY_PCNT", 0.01)
        config.put("AUTO_SENSITIVITY_SD", 3.0)

        ModelAdapter ma = ProcessableObjectFactory.buildTSModel(ts, config)
        AnomalyDetector ad = ProcessableObjectFactory.buildAnomalyModel(ts, config)
        OnlineAnomalyDetector onlineAnomalyDetector = new OnlineAnomalyDetector(ma, ad)
        TimeSeries.DataSequence result = onlineAnomalyDetector.predict()

        double expect = result.values.last()
        return round(expect, 4)
    }

    public static List<Long> genHourTimestamp(int year, int month) {
        Calendar calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, Calendar.JANUARY + month)

        calendar.clear(Calendar.MILLISECOND)
        calendar.clear(Calendar.SECOND)
        calendar.clear(Calendar.MINUTE)

        int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)

        List<Long> result = new ArrayList<>()
        (1..days).forEach { day ->
            calendar.set(Calendar.DAY_OF_MONTH, day)
            (0..23).forEach { hour ->
                calendar.set(Calendar.HOUR_OF_DAY, hour)
                result.add(calendar.getTimeInMillis())
            }
        }
        return result
    }

    /**
     * 产生滑动窗口的逐小时 timestamp, 不包含 date 所在月份
     * @param date
     * @return
     */
    public static List<Long> genPreviousMonthTimestamp(Date date, int windowSize = 1) {
        if (windowSize < 0)
            throw new IllegalArgumentException("windowSize has to be non negative")
        List<Long> result = new ArrayList<>()
        if (windowSize == 0)
            return result
        Calendar calendar = Calendar.getInstance()
        calendar.setTime(date)
        int startYear = calendar.get(Calendar.YEAR) // 当前年份
        int startMonth = calendar.get(Calendar.MONTH) - 1 // 当前月份, 从 0 开始
        List<Tuple2<Integer, Integer>> yearMonth = new LinkedList<>()
        for (int i = 0; i < windowSize; i++) {
            if (startMonth == -1) {
                startYear -= 1
                startMonth = 11
            }
            yearMonth.addFirst(new Tuple2<Integer, Integer>(startYear, startMonth))
            startMonth -= 1
        }
        yearMonth.forEach { it ->
            result.addAll(genHourTimestamp(it.first, it.second))
        }
        return result
    }

    public static List<Long> genAfterMonthTimestamp(Date date, int windowSize = 1) {
        if (windowSize < 0)
            throw new IllegalArgumentException("windowSize has to be non negative")
        List<Long> result = new ArrayList<>()
        if (windowSize == 0)
            return result
        Calendar calendar = Calendar.getInstance()
        calendar.setTime(date)
        int startYear = calendar.get(Calendar.YEAR) // 当前年份
        int startMonth = calendar.get(Calendar.MONTH) + 1 // 当前月份, 从 0 开始
        List<Tuple2<Integer, Integer>> yearMonth = new ArrayList<>()
        for (int i = 0; i < windowSize; i++) {
            if (startMonth > 11) {
                startMonth == 0
                startYear += 1
            }
            yearMonth.add(new Tuple2<Integer, Integer>(startYear, startMonth))
            startMonth += 1
        }
        yearMonth.forEach { it ->
            result.addAll(genHourTimestamp(it.first, it.second))
        }
        return result
    }

    public static long genDayStartTimestamp(Date date) {
        Calendar calendar = Calendar.getInstance()
        calendar.setTime(date)
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.clear(Calendar.MINUTE)
        calendar.clear(Calendar.SECOND)
        calendar.clear(Calendar.MILLISECOND)
        return calendar.timeInMillis
    }

    public static long genDayEndTimestamp(Date date) {
        Calendar calendar = Calendar.getInstance()
        calendar.setTime(date)
        calendar.set(Calendar.HOUR_OF_DAY, 23)
        calendar.clear(Calendar.MINUTE)
        calendar.clear(Calendar.SECOND)
        calendar.clear(Calendar.MILLISECOND)
        return calendar.timeInMillis
    }

    /**
     * 离线检测
     * @param entityId
     * @return
     */
    public static List<FourTuple> offlineDetect(String entityId, Date date, ManagementEntityMapper entityMapper) {
        Calendar calendar = Calendar.getInstance()
        calendar.setTime(date)

        List<FourTuple> tuples = new ArrayList<>()
        final String tableName = TableManager.getTable(entityId, TableManager.TableType.Hour)

        List<Long> previousMonthTimestamp = genPreviousMonthTimestamp(date, 1)
        List<FourTuple> previousMonthTuples = previousMonthTimestamp.stream().map { timestamp ->
            entityMapper.getFourTuple(tableName, entityId, timestamp)
        }.filter { it != null }.collect(Collectors.toList())
        tuples.addAll(previousMonthTuples)

        int currentYear = calendar.get(Calendar.YEAR)
        int currentMonth = calendar.get(Calendar.MONTH)
        int currentDay = calendar.get(Calendar.DAY_OF_MONTH)
        List<Long> currentMonthTimestamp = genHourTimestamp(currentYear, currentMonth)
        List<FourTuple> currentMonthTuples = currentMonthTimestamp.stream().map { timestamp ->
            entityMapper.getFourTuple(tableName, entityId, timestamp)
        }.filter { it != null }.collect(Collectors.toList())
        tuples.addAll(currentMonthTuples)

        long dateStartTimestamp = genDayStartTimestamp(date), dateEndTimestamp = genDayEndTimestamp(date)
        List<FourTuple> currentDateTuples = currentMonthTuples.stream().filter { tuple ->
            long timestamp = tuple.getTimestamp()
            timestamp >= dateStartTimestamp && timestamp <= dateEndTimestamp
        }.collect(Collectors.toList())

        List<Long> nextMonthTimestamp = genAfterMonthTimestamp(date, 1)
        List<FourTuple> nextMonthTuples = nextMonthTimestamp.stream().map { timestamp ->
            entityMapper.getFourTuple(tableName, entityId, timestamp)
        }.filter { it != null }.collect(Collectors.toList())
        tuples.addAll(nextMonthTuples)

        if (tuples.isEmpty())
            return Collections.emptyList()

        // 排序
        tuples.sort(new Comparator<FourTuple>() {
            @Override
            int compare(FourTuple o1, FourTuple o2) {
                return Long.compare(o1.getTimestamp(), o2.getTimestamp())
            }
        })

        offlineAnomalyLogger.info("[预测]: ID: " + entityId + ", 时间戳: " + date.format("yyyy-MM-dd") +
                ", 训练数据起始时间戳: " + milliSecond2DateStr(tuples.first().getTimestamp()) + ", 训练数据终止时间戳:" + milliSecond2DateStr(tuples.last().getTimestamp()))

        // 基于 tuples 构建 TimeSeries
        TimeSeries ts = new TimeSeries()
        tuples.forEach { it ->
            long time = millis2sec(it.getTimestamp())
            long value = it.getValue().toFloat()
            ts.append(time, value)
        }

        Properties config = new Properties()
        config.put("DETECTION_WINDOW_START_TIME", "0")
        config.put("MAX_ANOMALY_TIME_AGO", "999999999")
        config.put("AGGREGATION", "1")

        config.put("OP_TYPE", "DETECT_ANOMALY")

        config.put("TS_MODEL", "OlympicModel")

        config.put("AD_MODEL", "ExtremeLowDensityModel")

        // config for Olympic Forecast Model
        config.put("TIME_SHIFTS", "0,1")
        config.put("BASE_WINDOWS", "1,1")
        config.put("PERIOD", "0")
        config.put("NUM_WEEKS", "2")
        config.put("NUM_TO_DROP", "0")
        config.put("DYNAMIC_PARAMETERS", "1")

        // config for ExtremeLowDensityModel & DBScanModel
        config.put("AUTO_SENSITIVITY_ANOMALY_PCNT", 0.01)
        config.put("AUTO_SENSITIVITY_SD", 3.0)

        ModelAdapter ma = ProcessableObjectFactory.buildTSModel(ts, config)
        AnomalyDetector ad = ProcessableObjectFactory.buildAnomalyModel(ts, config)
        OfflineAnomalyDetector offlineAnomalyDetector = new OfflineAnomalyDetector(ma, ad)
        TimeSeries.DataSequence result = offlineAnomalyDetector.detect()

//        List<Long> currentDateTimestamp = new ArrayList<>()
//        Calendar t = Calendar.getInstance()
//        t.set(Calendar.YEAR, currentYear)
//        t.set(Calendar.MONTH, currentMonth)
//        t.set(Calendar.DAY_OF_MONTH, currentDay)
//
//        (0..23).forEach{ hour ->
//            t.set(Calendar.HOUR_OF_DAY,hour)
//            currentDateTimestamp.add(t.getTimeInMillis())
//        }

        long firstTimestampInSec = millis2sec(currentDateTuples.first().getTimestamp())
        Float[] values = result.values
        Long[] times = result.times
        int i = 0;
        for (; i < times.length; i++) {
            if (times[i] == firstTimestampInSec)
                break
        }
        if (i == times.length) {
            offlineAnomalyLogger.error("Cannot find ")
            return Collections.emptyList()
        }

        for (int j = 0; i + j < times.length && j < currentDateTuples.size(); j++) {
            FourTuple tuple = currentDateTuples.get(j)
            tuple.setExpect(values[i + j])
            currentDateTuples.set(j, tuple)
        }

        return currentDateTuples
    }
}
