package com.me.connector.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by wqlin on 18-4-17 10:25.
 */
@Configuration
@ComponentScan({"com.me.connector"})
public class RootConfig {
    @Bean
    public ExecutorService createExecutorService(){
        return Executors.newFixedThreadPool(4);
    }
}
